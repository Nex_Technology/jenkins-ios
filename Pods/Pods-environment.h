
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 2
#define COCOAPODS_VERSION_MINOR_AFNetworking 0
#define COCOAPODS_VERSION_PATCH_AFNetworking 1

// AFNetworking/NSURLConnection
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLConnection
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLConnection 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLConnection 0
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLConnection 1

// AFNetworking/NSURLSession
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLSession
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLSession 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLSession 0
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLSession 1

// AFNetworking/Reachability
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Reachability
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Reachability 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Reachability 0
#define COCOAPODS_VERSION_PATCH_AFNetworking_Reachability 1

// AFNetworking/Security
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Security
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Security 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Security 0
#define COCOAPODS_VERSION_PATCH_AFNetworking_Security 1

// AFNetworking/Serialization
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Serialization
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Serialization 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Serialization 0
#define COCOAPODS_VERSION_PATCH_AFNetworking_Serialization 1

// AFNetworking/UIKit
#define COCOAPODS_POD_AVAILABLE_AFNetworking_UIKit
#define COCOAPODS_VERSION_MAJOR_AFNetworking_UIKit 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_UIKit 0
#define COCOAPODS_VERSION_PATCH_AFNetworking_UIKit 1

// CRNavigationController
#define COCOAPODS_POD_AVAILABLE_CRNavigationController
#define COCOAPODS_VERSION_MAJOR_CRNavigationController 0
#define COCOAPODS_VERSION_MINOR_CRNavigationController 0
#define COCOAPODS_VERSION_PATCH_CRNavigationController 1

// Facebook-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_Facebook_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_Facebook_iOS_SDK 3
#define COCOAPODS_VERSION_MINOR_Facebook_iOS_SDK 9
#define COCOAPODS_VERSION_PATCH_Facebook_iOS_SDK 0

// JSONKit
#define COCOAPODS_POD_AVAILABLE_JSONKit
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 1.5pre.

// MagicalRecord
#define COCOAPODS_POD_AVAILABLE_MagicalRecord
#define COCOAPODS_VERSION_MAJOR_MagicalRecord 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord 2
#define COCOAPODS_VERSION_PATCH_MagicalRecord 0

// QuickDialog
#define COCOAPODS_POD_AVAILABLE_QuickDialog
#define COCOAPODS_VERSION_MAJOR_QuickDialog 0
#define COCOAPODS_VERSION_MINOR_QuickDialog 9
#define COCOAPODS_VERSION_PATCH_QuickDialog 0

// SDWebImage
#define COCOAPODS_POD_AVAILABLE_SDWebImage
#define COCOAPODS_VERSION_MAJOR_SDWebImage 3
#define COCOAPODS_VERSION_MINOR_SDWebImage 5
#define COCOAPODS_VERSION_PATCH_SDWebImage 0

// SDWebImage/Core
#define COCOAPODS_POD_AVAILABLE_SDWebImage_Core
#define COCOAPODS_VERSION_MAJOR_SDWebImage_Core 3
#define COCOAPODS_VERSION_MINOR_SDWebImage_Core 5
#define COCOAPODS_VERSION_PATCH_SDWebImage_Core 0

// SVProgressHUD
#define COCOAPODS_POD_AVAILABLE_SVProgressHUD
#define COCOAPODS_VERSION_MAJOR_SVProgressHUD 0
#define COCOAPODS_VERSION_MINOR_SVProgressHUD 9
#define COCOAPODS_VERSION_PATCH_SVProgressHUD 0

// TSMiniWebBrowser
#define COCOAPODS_POD_AVAILABLE_TSMiniWebBrowser
#define COCOAPODS_VERSION_MAJOR_TSMiniWebBrowser 1
#define COCOAPODS_VERSION_MINOR_TSMiniWebBrowser 0
#define COCOAPODS_VERSION_PATCH_TSMiniWebBrowser 1

// TTTAttributedLabel
#define COCOAPODS_POD_AVAILABLE_TTTAttributedLabel
#define COCOAPODS_VERSION_MAJOR_TTTAttributedLabel 1
#define COCOAPODS_VERSION_MINOR_TTTAttributedLabel 7
#define COCOAPODS_VERSION_PATCH_TTTAttributedLabel 5

