//
//  AppDelegate.m
//  IACTAC
//
//  Created by Zayar on 10/29/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "AppDelegate.h"
#import "Utility.h"
#import "JSONKit.h"
#import "News.h"
#import "Stores.h"
#import "Product_Category.h"
#import "Products.h"
#import "Facebook.h"
#import "News.h"
#import "IacAPIClient.h"
#import "StringTable.h"
#import "Config.h"
#import "Config_Tabs.h"
static NSString* kAppId = @"371097079691179";
NSString *const FBSessionStateChangedNotification =@"com.nex.tac:FBSessionStateChangedNotification";
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    //[self uiSetup];
    [self setupDB];
    
    [self getUIConfigAndSetup];
    //[self uiSetup];
    
    return YES;
}

- (void)uiSetup{
    NSMutableArray * arrViews = [[NSMutableArray alloc] init];
    NSArray * arrTabs = [Config_Tabs findAllSortedBy:@"order_id" ascending:YES];
    for (Config_Tabs * objTab in arrTabs) {
        UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:objTab.title];
        [arrViews addObject:nav];
    }
    UITabBarController * tabViewController = [[UITabBarController alloc] init];
    [tabViewController setViewControllers:arrViews animated:NO];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = tabViewController;
    [self.window makeKeyAndVisible];
    
    NSLog(@"check point twoo arr count %d",[[Config_Tabs findAll] count]);
    
    NSUserDefaults * user_default = [NSUserDefaults standardUserDefaults];
    NSString * strAppId = [user_default objectForKey:APP_TITLE];
    //NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"app_id == %@",strAppId];
    Config * config = [Config MR_findFirstWithPredicate:predicate];
    if ([Utility isLessOSVersion:@"7.0"])
        [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithHexString:config.tab_selected_color]];
    else if([Utility isGreaterOREqualOSVersion:@"7.0"])
        [[UITabBar appearance] setTintColor:[UIColor colorWithHexString:config.tab_selected_color]];
    
    if ([[UINavigationBar class]respondsToSelector:@selector(appearance)]) {
        if([Utility isGreaterOREqualOSVersion:@"7.0"]){
            [[UINavigationBar appearance]setTintColor:[UIColor colorWithHexString:config.app_tint_color]]; // it set color of bar button item text
            //[[UINavigationBar appearance]setBarTintColor:[UIColor whiteColor]]; // it set color of navigation
            [[UINavigationBar appearance]setBackgroundColor:[UIColor colorWithHexString:config.nav_bg_color]];
        }
    }
    
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        self.window.tintColor = [UIColor colorWithHexString:config.app_tint_color];
    }
    
    [self syncNews];
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //[self syncNews];
}

- (void) setupDB
{
    NSLog(@"set up!!");
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"tac.sqlite"];
}

//////// share Facebook

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    NSLog(@"........");
    // FBSample logic
    // We need to handle URLs by passing them to FBSession in order for SSO authentication
    // to work.
    return [FBSession.activeSession handleOpenURL:url];
}

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState)state
                      error:(NSError *)error
{
    NSLog(@"sessionStateChanged");
    // FBSample logic
    // Any time the session is closed, we want to display the login controller (the user
    // cannot use the application unless they are logged in to Facebook). When the session
    // is opened successfully, hide the login controller and show the main UI.
    switch (state) {
        case FBSessionStateOpen: {
            /*UIViewController *topViewController = [self.navController topViewController];
             if ([[topViewController modalViewController] isKindOfClass:[SCLoginViewController class]]) {
             [topViewController dismissModalViewControllerAnimated:YES];
             }*/
            // Initiate a Facebook instance and properties
            if (nil == self.facebook) {
                self.facebook = [[Facebook alloc]
                                 initWithAppId:FBSession.activeSession.appID
                                 andDelegate:nil];
                
                // Store the Facebook session information
                NSLog(@"accesstoken %@ expirationDate %@ appid %@",FBSession.activeSession.accessToken,FBSession.activeSession.expirationDate,FBSession.activeSession.appID);
                self.facebook.accessToken = FBSession.activeSession.accessToken;
                self.facebook.expirationDate = FBSession.activeSession.expirationDate;
            }
            NSLog(@"Great Back!!!<<<<>>>");
            
            //here is need to show up
            //[self showDialog];
            
            // FBSample logic
            // Pre-fetch and cache the friends for the friend picker as soon as possible to improve
            // responsiveness when the user tags their friends.
            FBCacheDescriptor *cacheDescriptor = [FBFriendPickerViewController cacheDescriptor];
            [cacheDescriptor prefetchAndCacheForSession:session];
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            // FBSample logic
            // Once the user has logged in, we want them to be looking at the root view.
            //[self.navController popToRootViewControllerAnimated:NO];
            
            [FBSession.activeSession closeAndClearTokenInformation];
            
            //[self showLoginView];
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:FBSessionStateChangedNotification
                                                        object:session];
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    
    
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    
    NSArray *permissions = [NSArray arrayWithObjects:@"publish_actions",@"user_photos", nil];
    return [FBSession openActiveSessionWithPermissions:permissions
                                          allowLoginUI:allowLoginUI
                                     completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                         [self sessionStateChanged:session state:state error:error];
                                     }];
    
}
/**
 * A function for parsing URL parameters.
 */

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [[kv objectAtIndex:1]
         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;
}

// Handle the publish feed call back

- (void) dialogCompleteWithUrl:(NSURL *)url {
    
    NSLog(@"dialogCompleteWithUrl");
    NSDictionary *params = [self parseURLParams:[url query]];
    NSString *msg = [NSString stringWithFormat:
                     @"Posted story, id: %@",
                     [params valueForKey:@"post_id"]];
    NSLog(@"%@", msg);
    // Show the result in an alert
    
    //    if(![msg isEqualToString:@""] || msg!=nil || (NSNull *)msg != [NSNull null]){
    //    }
    /*   [[[UIAlertView alloc] initWithTitle:@"Result"
     message:msg
     delegate:nil
     cancelButtonTitle:@"OK!"
     otherButtonTitles:nil]
     show];*/
    
    
}

- (void)fbShare:(News *)obj{
    
    /*if (![self.facebook isSessionValid]) {
        NSArray *permis = [NSArray arrayWithObjects:@"publish_actions",@"user_photos", nil];
        //,@"first_name",@"last_name",@"gender"
        [self openSessionWithAllowLoginUI:YES];
    } else {
        [self showDialogWith:obj];
    }*/
    
    
}

/*- (void) showDialogWith:(News *)objLF{
   
   // SBJSON *jsonWriter = [SBJSON new];
    NSDictionary *propertyvalue = [NSDictionary dictionaryWithObjectsAndKeys:@"Pet App", @"text", @"http://www.petfinder.com", @"href", nil];
    NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:propertyvalue, @"Via", nil];
    NSString *finalactions = [jsonWriter stringWithObject:properties];
    
    NSMutableDictionary *params;
    params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
              objLF.strPetName, @"name", @"" ,@"caption",
              [NSString stringWithFormat:@"%@, Reward: %0.2f",objLF.strDescription,objLF.reward] ,@"description",
              @"http://www.petsmagazine.com.sg/" , @"link",
              objLF.strPetImgLink , @"picture",
              finalactions,@"properties",
              nil];
    // Put together the dialog parameters
    
    // Invoke the dialog
    [self.facebook dialog:@"feed" andParams:params andDelegate:self];
}*/

- (void) showDialogWith:(News *)obj{
    // Put together the dialog parameters
    NSMutableDictionary *params =
    [NSMutableDictionary dictionaryWithObjectsAndKeys:
     @"TAC", @"name",
     @"The Analytic Company", @"caption",
     @"The Analytics Company, TAC in short, is a leading provider and distributor of world-class IT products &amp; solutions in Myanmar. We have enabled our clients to successfully implement best-in-class IT solutions and technologies across various industries. We partner with Tableau Software, Symantec, Datawatch and Splunk.", @"description",
     obj.url, @"link",
     obj.image_url, @"picture",
     nil];
    
    // Invoke the dialog
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:
     ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
         if (error) {
             // Error launching the dialog or publishing a story.
             NSLog(@"Error publishing story.");
         } else {
             if (result == FBWebDialogResultDialogNotCompleted) {
                 // User clicked the "x" icon
                 NSLog(@"User canceled story publishing.");
             } else {
                 // Handle the publish feed callback
                 NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                 if (![urlParams valueForKey:@"post_id"]) {
                     // User clicked the Cancel button
                     NSLog(@"User canceled story publishing.");
                 } else {
                     // User clicked the Share button
                     NSString *msg = [NSString stringWithFormat:
                                      @"Posted story, id: %@",
                                      [urlParams valueForKey:@"post_id"]];
                     NSLog(@"%@", msg);
                     // Show the result in an alert
                     [[[UIAlertView alloc] initWithTitle:@"Result"
                                                 message:msg
                                                delegate:nil
                                       cancelButtonTitle:@"OK!"
                                       otherButtonTitles:nil]
                      show];
                 }
             }
         }
     }];
}

- (void) getUIConfigAndSetup{
    NSUserDefaults * user_default = [NSUserDefaults standardUserDefaults];
    NSString * strAppId = [user_default objectForKey:APP_TITLE];
    //NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"app_id == %@",strAppId];
    Config * config = [Config MR_findFirstWithPredicate:predicate];
    NSLog(@"config link %@",config.conf_link);
    if (config) {
        
        [[IacAPIClient sharedClientWithoutBaseUrl] GET:config.conf_link parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
            NSLog(@"config successfully return!!! %@",json);
            [SVProgressHUD dismiss];
            NSDictionary * dicConfig = [(NSDictionary *)json objectForKey:@"config"];
            [self saveAndUpdateConfig:dicConfig];
            [self uiSetup];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"serverConfigLoaded" object:nil];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [SVProgressHUD dismiss];
            NSLog(@"config Error %@",error);
        }];
    }
    else{
        NSError *error;
        NSBundle *mainBundle = [NSBundle mainBundle];
        NSString *myFile = [mainBundle pathForResource: @"config" ofType: @"json"];
        NSString *response=[NSString stringWithContentsOfFile:myFile
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
        if (error)
        {
            NSLog(@"config error %@", error);
        }
        
        //NSString *validJSON = [self convertResponseIntoValidJSON:response];
        NSData *soapData = [response dataUsingEncoding:NSUTF8StringEncoding];
        
        JSONDecoder* decoder = [[JSONDecoder alloc]
                                initWithParseOptions:JKParseOptionNone];
        NSDictionary* myDict = [decoder objectWithData:soapData];
        NSDictionary * dicConfig = [myDict objectForKey:@"config"];
        [self saveAndUpdateConfig:dicConfig];
        [self uiSetup];
    }
   
}

- (void)saveAndUpdateConfig:(NSDictionary *)dicConfig{
    NSLog(@"config %@",dicConfig);
    NSUserDefaults * prefe = [NSUserDefaults standardUserDefaults];
    NSString * strAppId = [prefe objectForKey:APP_TITLE];
    
    /*
     "app_id": "0123456789",
     "base_bg": "#000000",
     "app_tint_color": "#000000",
     "nav_bg_color": "#000000",
     "nav_title_color": "#000000",
     "nav_title": "test",
     "nav_logo_link": "test",
     "nav_is_transprent": 0,
     "tab_selected_color": "#000000",
     */
    NSString * strId = [dicConfig objectForKey:@"app_id"];
    NSString * strBaseBg = [dicConfig objectForKey:@"base_bg"];
    NSString * strTintColor = [dicConfig objectForKey:@"app_tint_color"];
    NSString * strNavBgColor = [dicConfig objectForKey:@"nav_bg_color"];
    NSString * strNavTitleColor = [dicConfig objectForKey:@"nav_title_color"];
    NSString * strNavTitle = [dicConfig objectForKey:@"nav_title"];
    NSString * strNavLogo = [dicConfig objectForKey:@"nav_title_color"];
    int navIsTransprent = [[dicConfig objectForKey:@"nav_is_transprent"] integerValue];
    int canOrder = [[dicConfig objectForKey:@"can_order"] integerValue];
    NSString * strConfigLink = [dicConfig objectForKey:@"config_url"];
    NSString * strTabSelectedColor = [dicConfig objectForKey:@"tab_selected_color"];
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"app_id == %@",strAppId];
    [prefe setObject:strId forKey:APP_TITLE];
    [prefe synchronize];
    Config * config = [Config findFirstWithPredicate:predicate inContext:localContext];
    if (config) {
        NSLog(@"existing config!!");
        config.app_id = strId;
        config.base_bg = strBaseBg;
        config.app_tint_color = strTintColor;
        config.nav_bg_color = strNavBgColor;
        config.nav_title_color = strNavTitleColor;
        config.nav_title = strNavTitle;
        config.nav_logo_link = strNavLogo;
        config.nav_is_transparent = [NSNumber numberWithInt:navIsTransprent];
        config.tab_selected_color = strTabSelectedColor;
        config.can_order = [NSNumber numberWithInt:canOrder];
        config.conf_link = strConfigLink;
        [self updateConfig:config withLocalContext:localContext];
    }
    else{
        NSLog(@"start new config!!");
        Config * configNew = [Config MR_createEntity];
        configNew.app_id = strId;
        configNew.base_bg = strBaseBg;
        configNew.app_tint_color = strTintColor;
        configNew.nav_bg_color = strNavBgColor;
        configNew.nav_title_color = strNavTitleColor;
        configNew.nav_title = strNavTitle;
        configNew.nav_logo_link = strNavLogo;
        configNew.nav_is_transparent = [NSNumber numberWithInt:navIsTransprent];
        configNew.can_order = [NSNumber numberWithInt:canOrder];
        configNew.tab_selected_color = strTabSelectedColor;
        configNew.conf_link = strConfigLink;
        [self insertConfig:configNew withLocalContext:localContext];
    }
    NSLog(@"config app id %@",strId);
    
    NSArray * arr = [dicConfig objectForKey:@"tabs"];
    if ([arr count]>0) {
        [Config_Tabs truncateAllInContext:localContext];
    }
    for (NSDictionary * dic in arr) {
        /*
         "api_url": "http: //tacapi.herokuapp.com/api/products",
         "order": 1,
         "title": "Products",
         "type": 1,
         "tab_icon": "http: //tacapi.herokuapp.com/products_tab_icon.png"
         */
        NSString * strApiURL = [dic objectForKey:@"api_url"];
        int order = [[dic objectForKey:@"order"] integerValue];
        NSString * strTitle= [dic objectForKey:@"title"];
        int type = [[dic objectForKey:@"type"] integerValue];
        NSString * strIcon = [dic objectForKey:@"tab_icon"];
        NSString * strTagId = [dic objectForKey:@"tab_id"];
        
        Config_Tabs * configTab = [Config_Tabs MR_createEntity];
        configTab.tab_id = strTagId;
        configTab.api_url = strApiURL;
        configTab.order_id = [NSNumber numberWithInt:order];
        configTab.type = [NSNumber numberWithInt:type];
        configTab.title = strTitle;
        configTab.icon = strIcon;
        configTab.app_id = strId;
        [self insertConfigTab:configTab withLocalContext:localContext];
    }
    NSLog(@"check point arr count %d",[[Config_Tabs findAll] count]);
}

- (void) updateConfig:(Config *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    
    // Create a new Person in the current thread context
    //obj                          = [Employee MR_createInContext:localContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"app_id ==[c] %@ ", obj.app_id];
    Config * employeeFounded = [Config MR_findFirstWithPredicate:predicate inContext:localContext];
    if (employeeFounded) {
        /*[localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
         Employee * objEmp = [Employee MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
         NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
         }];*/
        employeeFounded = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            NSLog(@"Config updated!!");
        }];
    }
}

- (void) insertConfig:(Config *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    // Get the local context
    // Create a new Person in the current thread context
    //Employee *person                          = [Employee MR_createInContext:localContext];
    //person = obj;
    [obj MR_inContext:localContext];
    
    // Save the modification in the local context
    [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        NSLog(@"Config saved");
    }];
}

- (void) insertConfigTab:(Config_Tabs *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    // Get the local context
    // Create a new Person in the current thread context
    //Employee *person                          = [Employee MR_createInContext:localContext];
    //person = obj;
    [obj MR_inContext:localContext];
    
    // Save the modification in the local context
    [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        NSLog(@"Config_Tabs saved");
    }];
}

#pragma News Sync Methods
- (void) syncNews{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tab_id == %@",TAB_NEWS_ID];
    Config_Tabs * tab_news = [Config_Tabs MR_findFirstWithPredicate:predicate];
    if (![Utility stringIsEmpty:tab_news.api_url shouldCleanWhiteSpace:YES]) {
        [[IacAPIClient sharedClientWithoutBaseUrl] GET:tab_news.api_url parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
            [SVProgressHUD dismiss];
            NSLog(@"News successfully return!!! %@",json);
            NSDictionary * myDict = (NSDictionary *)json;
            NSString * strServerId = @"";
            NSString * strLocalId = @"";
            //NSArray * arrNews = [myDict objectForKey:@"news"];
            NSArray * arrNews = (NSArray *)myDict;
            if ([arrNews count]) {
                [News MR_truncateAll];
            }
            NSLog(@"arr news count %d",[arrNews count]);
            for(NSInteger i=0;i<[arrNews count];i++){
                NSDictionary * dicEmp = (NSDictionary *)[arrNews objectAtIndex:i];
                strServerId = [dicEmp objectForKey:@"newsID"];
                //strLocalId = [dicEmp objectForKey:@"local_id"];
                strLocalId = @" ";
                
                NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"server_id == %@",strServerId];
                News * emp = [News findFirstWithPredicate:predicate inContext:localContext];
                if (emp != nil) {
                    //emp.server_id = [dics objectForKey:@"newsID"];
                    //int ser_id = [[dicEmp objectForKey:@"newsID"] intValue];
                    emp.server_id = [dicEmp objectForKey:@"newsID"];
                    emp.title = [dicEmp objectForKey:@"newsTitle"];
                    emp.author = [dicEmp objectForKey:@"newsAuthor"];
                    [emp setTheDateFromString:[dicEmp objectForKey:@"newsDate"]];
                    emp.author = [dicEmp objectForKey:@"newsAuthor"];
                    emp.short_descript = [dicEmp objectForKey:@"newsShortDesc"];
                    emp.content = [dicEmp objectForKey:@"content"];
                    emp.image_url = [dicEmp objectForKey:@"newsImage"];
                    emp.url = [dicEmp objectForKey:@"newsUrl"];
                    emp.category = [dicEmp objectForKey:@"categoryTitle"];
                    emp.updated_time = [NSNumber numberWithInt:[[dicEmp objectForKey:@"updated_at"] intValue]];
                    emp.is_active = [NSNumber numberWithInt:[[dicEmp objectForKey:@"is_active"] intValue]];
                    emp.has_sync = [NSNumber numberWithInt:1];
                    emp.timetick = [NSNumber numberWithInt:[[dicEmp objectForKey:@"newsTimestamp"] intValue]];
                    [self updateNews:emp withLocalContext:localContext];
                }else{
                    News * objEmp2 = [News MR_createEntity];
                    //int ser_id = [[dicEmp objectForKey:@"newsID"] intValue];
                    //objEmp2.server_id = [NSString stringWithFormat:@"%d",ser_id];
                    objEmp2.server_id = [dicEmp objectForKey:@"newsID"];
                    objEmp2.title = [dicEmp objectForKey:@"newsTitle"];
                    objEmp2.author = [dicEmp objectForKey:@"newsAuthor"];
                    [objEmp2 setTheDateFromString:[dicEmp objectForKey:@"newsDate"]];
                    objEmp2.author = [dicEmp objectForKey:@"newsAuthor"];
                    objEmp2.short_descript = [dicEmp objectForKey:@"newsShortDesc"];
                    objEmp2.content = [dicEmp objectForKey:@"content"];
                    objEmp2.image_url = [dicEmp objectForKey:@"newsImage"];
                    objEmp2.url = [dicEmp objectForKey:@"newsUrl"];
                    objEmp2.category = [dicEmp objectForKey:@"categoryTitle"];
                    objEmp2.updated_time = [NSNumber numberWithInt:[[dicEmp objectForKey:@"updated_at"] intValue]];
                    objEmp2.is_active = [NSNumber numberWithInt:[[dicEmp objectForKey:@"is_active"] intValue]];
                    objEmp2.has_sync = [NSNumber numberWithInt:1];
                    objEmp2.timetick = [NSNumber numberWithInt:[[dicEmp objectForKey:@"newsTimestamp"] intValue]];
                    [self insertNews:objEmp2 withLocalContext:localContext];
                }
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [SVProgressHUD dismiss];
            NSLog(@"Error %@",error);
            //[SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
            //[SVProgressHUD dismiss];
        }];
        
        [self syncProducts];
    }
}

- (void) saveORupdateNewsWithServerId:(NSString *)strServerId andLocalId:(NSString *)strLocalId andDics:(NSDictionary *)dics{
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
    //[strLocalId isEqualToString:@"0"];
    /*if (![Utility stringIsEmpty:strLocalId shouldCleanWhiteSpace:YES]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"local_id == %@",strLocalId];
        News * emp = [News findFirstWithPredicate:predicate inContext:localContext];
        if (emp != nil) {
            int ser_id = [[dics objectForKey:@"newsID"] intValue];
            emp.server_id = [NSString stringWithFormat:@"%d",ser_id];
            emp.title = [dics objectForKey:@"newsTitle"];
            emp.author = [dics objectForKey:@"newsAuthor"];
            [emp setTheDateFromString:[dics objectForKey:@"newsDate"]];
            emp.author = [dics objectForKey:@"newsAuthor"];
            emp.short_descript = [dics objectForKey:@"newsShortDesc"];
            emp.content = [dics objectForKey:@"newsContent"];
            emp.image_url = [dics objectForKey:@"newsImage"];
            emp.url = [dics objectForKey:@"newsUrl"];
            emp.updated_time = [NSNumber numberWithInt:[[dics objectForKey:@"updated_at"] intValue]];
            emp.is_active = [NSNumber numberWithInt:[[dics objectForKey:@"is_active"] intValue]];
            emp.has_sync = [NSNumber numberWithInt:1];
            emp.timetick = [NSNumber numberWithInt:[[dics objectForKey:@"newsTimestamp"] intValue]];
            
            [self updateNewsWithLocalID:emp withLocalContext:localContext];
        }
    }
    else*/
    if(![Utility stringIsEmpty:strServerId shouldCleanWhiteSpace:YES]){
        //NSLog(@"server_id %@",strServerId);
        /*NSPredicate *predicate = [NSPredicate predicateWithFormat:@"server_id == %@",strServerId];
        News * emp = [News findFirstWithPredicate:predicate inContext:localContext];
        if (emp != nil) {
            
            //emp.server_id = [dics objectForKey:@"newsID"];
            int ser_id = [[dics objectForKey:@"newsID"] intValue];
            emp.server_id = [NSString stringWithFormat:@"%d",ser_id];
            emp.title = [dics objectForKey:@"newsTitle"];
            emp.author = [dics objectForKey:@"newsAuthor"];
            [emp setTheDateFromString:[dics objectForKey:@"newsDate"]];
            emp.author = [dics objectForKey:@"newsAuthor"];
            emp.short_descript = [dics objectForKey:@"newsShortDesc"];
            emp.content = [dics objectForKey:@"newsContent"];
            emp.image_url = [dics objectForKey:@"newsImage"];
            emp.url = [dics objectForKey:@"newsUrl"];
            emp.updated_time = [NSNumber numberWithInt:[[dics objectForKey:@"updated_at"] intValue]];
            emp.is_active = [NSNumber numberWithInt:[[dics objectForKey:@"is_active"] intValue]];
            emp.has_sync = [NSNumber numberWithInt:1];
            emp.timetick = [NSNumber numberWithInt:[[dics objectForKey:@"newsTimestamp"] intValue]];
            [self updateNews:emp withLocalContext:localContext];
        }
        else{
            News * objEmp2 = [News createEntity];
            //objEmp2.server_id = [dics objectForKey:@"newsID"];
            int ser_id = [[dics objectForKey:@"newsID"] intValue];
            emp.server_id = [NSString stringWithFormat:@"%d",ser_id];
            objEmp2.title = [dics objectForKey:@"newsTitle"];
            objEmp2.author = [dics objectForKey:@"newsAuthor"];
            [objEmp2 setTheDateFromString:[dics objectForKey:@"newsDate"]];
            objEmp2.author = [dics objectForKey:@"newsAuthor"];
            objEmp2.short_descript = [dics objectForKey:@"newsShortDesc"];
            objEmp2.content = [dics objectForKey:@"newsContent"];
            objEmp2.image_url = [dics objectForKey:@"newsImage"];
            objEmp2.url = [dics objectForKey:@"newsUrl"];
            objEmp2.updated_time = [NSNumber numberWithInt:[[dics objectForKey:@"updated_at"] intValue]];
            objEmp2.is_active = [NSNumber numberWithInt:[[dics objectForKey:@"is_active"] intValue]];
            objEmp2.has_sync = [NSNumber numberWithInt:1];
            objEmp2.timetick = [NSNumber numberWithInt:[[dics objectForKey:@"newsTimestamp"] intValue]];
            [self insertNews:objEmp2 withLocalContext:localContext];
        }*/
    }
}

- (void) updateNews:(News *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    
    // Create a new Person in the current thread context
    //obj                          = [Employee MR_createInContext:localContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"server_id ==[c] %@ ", obj.server_id];
    News * employeeFounded = [News MR_findFirstWithPredicate:predicate inContext:localContext];
    if (employeeFounded) {
        /*[localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
         Employee * objEmp = [Employee MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
         NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
         }];*/
        employeeFounded = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            News * objEmp = [News MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
            NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
        }];
    }
}

- (void) updateNewsWithLocalID:(News *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    
    // Create a new Person in the current thread context
    //obj                          = [Employee MR_createInContext:localContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"local_id ==[c] %@ ", obj.local_id];
    News * employeeFounded = [News MR_findFirstWithPredicate:predicate inContext:localContext];
    if (employeeFounded) {
        /*[localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
         Employee * objEmp = [Employee MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
         NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
         }];*/
        employeeFounded = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            News * objEmp = [News MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
            NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
        }];
    }
}

- (void) insertNews:(News *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    // Get the local context
    // Create a new Person in the current thread context
    //Employee *person                          = [Employee MR_createInContext:localContext];
    //person = obj;
    [obj MR_inContext:localContext];
    
    // Save the modification in the local context
    [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        NSLog(@"news saved");
    }];
}
    
#pragma Products Sync Methods
- (void) syncStores{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tab_id == %@",TAB_STORE_ID];
    Config_Tabs * tab = [Config_Tabs MR_findFirstWithPredicate:predicate];
    if (![Utility stringIsEmpty:tab.api_url shouldCleanWhiteSpace:YES]) {
        [[IacAPIClient sharedClient] GET:[NSString stringWithFormat:@"%@",tab.api_url ] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
            [SVProgressHUD dismiss];
            NSLog(@"successfully return!!! %@",json);
            NSDictionary * myDict = (NSDictionary *)json;
            NSString * strServerId = @"";
            NSString * strLocalId = @"";
            //NSArray * arrNews = [myDict objectForKey:@"news"];
            NSArray * arrNews = (NSArray *)myDict;
            NSLog(@"arr news count %d",[arrNews count]);
            for(NSInteger i=0;i<[arrNews count];i++){
                NSDictionary * dicEmp = (NSDictionary *)[arrNews objectAtIndex:i];
                strServerId = [dicEmp objectForKey:@"branchID"];
                //strLocalId = [dicEmp objectForKey:@"local_id"];
                strLocalId = @" ";
                //[self saveORupdateNewsWithServerId:strServerId andLocalId:strLocalId andDics:dicEmp];
                
                NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"server_id == %@",strServerId];
                Stores * emp = [Stores findFirstWithPredicate:predicate inContext:localContext];
                if (emp != nil) {
                    
                    //emp.server_id = [dics objectForKey:@"newsID"];
                    //int ser_id = [[dicEmp objectForKey:@"branchID"] intValue];
                    emp.server_id = [dicEmp objectForKey:@"branchID"];
                    emp.title = [dicEmp objectForKey:@"branchTitle"];
                    
                    emp.content = [dicEmp objectForKey:@"branchContent"];
                    emp.image_url = [dicEmp objectForKey:@"branchImage"];
                    emp.thumb = [dicEmp objectForKey:@"branchThumbnail"];
                    emp.location = [dicEmp objectForKey:@"branchLocation"];
                    emp.order = [NSNumber numberWithInt:[[dicEmp objectForKey:@"storeOrder"] intValue]];
                    /*NSString * strLat = [dicEmp objectForKey:@"branchLat"];
                     emp.lat = [NSDecimalNumber decimalNumberWithString:strLat];
                     
                     NSString * strLong = [dicEmp objectForKey:@"branchLatLng"];
                     emp.longtitude = [NSDecimalNumber decimalNumberWithString:strLong];*/
                    emp.lat = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",[dicEmp objectForKey:@"branchLat"]]];
                    emp.longtitude = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",[dicEmp objectForKey:@"branchLatLng"]]];
                    
                    emp.email= [dicEmp objectForKey:@"oneTouchEmail"];
                    emp.phone= [dicEmp objectForKey:@"call"];
                    emp.fb_link = [dicEmp objectForKey:@"fbPageLink"];
                    emp.fb_id = [dicEmp objectForKey:@"fbId"];
                    
                    //emp.updated_time = [NSNumber numberWithInt:[[dicEmp objectForKey:@"updated_at"] intValue]];
                    //emp.is_active = [NSNumber numberWithInt:[[dicEmp objectForKey:@"is_active"] intValue]];
                    emp.has_sync = [NSNumber numberWithInt:1];
                    //emp.timetick = [NSNumber numberWithInt:[[dicEmp objectForKey:@"newsTimestamp"] intValue]];
                    [self updateStore:emp withLocalContext:localContext];
                }else{
                    Stores * objEmp2 = [Stores createEntity];
                    //                int ser_id = [[dicEmp objectForKey:@"branchID"] intValue];
                    //                objEmp2.server_id = [NSString stringWithFormat:@"%d",ser_id];
                    objEmp2.server_id = [dicEmp objectForKey:@"branchID"];
                    objEmp2.title = [dicEmp objectForKey:@"branchTitle"];
                    
                    objEmp2.content = [dicEmp objectForKey:@"branchContent"];
                    objEmp2.image_url = [dicEmp objectForKey:@"branchImage"];
                    objEmp2.thumb = [dicEmp objectForKey:@"branchThumbnail"];
                    objEmp2.location = [dicEmp objectForKey:@"branchLocation"];
                    objEmp2.order = [NSNumber numberWithInt:[[dicEmp objectForKey:@"storeOrder"] intValue]];
                    objEmp2.fb_id = [dicEmp objectForKey:@"fbId"];
                    /*NSString * strLat = [dicEmp objectForKey:@"branchLat"];
                     objEmp2.lat = [NSDecimalNumber decimalNumberWithString:strLat];
                     
                     NSString * strLong = [dicEmp objectForKey:@"branchLatLng"];
                     objEmp2.longtitude = [NSDecimalNumber decimalNumberWithString:strLong];*/
                    objEmp2.lat = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",[dicEmp objectForKey:@"branchLat"]]];
                    emp.longtitude = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",[dicEmp objectForKey:@"branchLatLng"]]];
                    
                    objEmp2.email= [dicEmp objectForKey:@"oneTouchEmail"];
                    objEmp2.phone= [dicEmp objectForKey:@"call"];
                    objEmp2.fb_link = [dicEmp objectForKey:@"fbPageLink"];
                    
                    //objEmp2.updated_time = [NSNumber numberWithInt:[[dicEmp objectForKey:@"updated_at"] intValue]];
                    //objEmp2.is_active = [NSNumber numberWithInt:[[dicEmp objectForKey:@"is_active"] intValue]];
                    objEmp2.has_sync = [NSNumber numberWithInt:1];
                    
                    [self insertStores:objEmp2 withLocalContext:localContext];
                }
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [SVProgressHUD dismiss];
            NSLog(@"Error %@",error);
            //[SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
            //[SVProgressHUD dismiss];
        }];
        [self syncStores];
    }
}
    
- (void) updateStore:(Stores *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    
    // Create a new Person in the current thread context
    //obj                          = [Employee MR_createInContext:localContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"server_id ==[c] %@ ", obj.server_id];
    Stores * employeeFounded = [Stores MR_findFirstWithPredicate:predicate inContext:localContext];
    if (employeeFounded) {
        /*[localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
         Employee * objEmp = [Employee MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
         NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
         }];*/
        employeeFounded = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            Stores * objEmp = [News MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
            NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
        }];
    }
}
    
- (void) updateStoresWithLocalID:(Stores *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    
    // Create a new Person in the current thread context
    //obj                          = [Employee MR_createInContext:localContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"local_id ==[c] %@ ", obj.local_id];
    Stores * employeeFounded = [Stores MR_findFirstWithPredicate:predicate inContext:localContext];
    if (employeeFounded) {
        /*[localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
         Employee * objEmp = [Employee MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
         NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
         }];*/
        employeeFounded = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            Stores * objEmp = [Stores MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
            NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
        }];
    }
}
    
- (void) insertStores:(Stores *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    // Get the local context
    // Create a new Person in the current thread context
    //Employee *person                          = [Employee MR_createInContext:localContext];
    //person = obj;
    [obj MR_inContext:localContext];
    
    // Save the modification in the local context
    [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        NSLog(@"store saved");
    }];
}

#pragma Stores Sync Methods
- (void) syncProducts{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tab_id == %@",TAB_PRODUCT_ID];
    Config_Tabs * tab = [Config_Tabs MR_findFirstWithPredicate:predicate];
    if (![Utility stringIsEmpty:tab.api_url shouldCleanWhiteSpace:YES]) {
        [[IacAPIClient sharedClientWithoutBaseUrl] GET:[NSString stringWithFormat:@"%@",tab.api_url] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
            [SVProgressHUD dismiss];
            NSLog(@"successfully return!!! %@",json);
            NSDictionary * myDict = (NSDictionary *)json;
            NSString * strServerId = @"";
            NSString * strLocalId = @"";
            //NSArray * arrNews = [myDict objectForKey:@"news"];
            NSArray * arrNews = [myDict objectForKey:@"categories"];
            NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
            Config * conf = [Config getCurrentDefaultConfig];
            conf.order_link = [myDict objectForKey:@"order_url"];
            [self updateConfig:conf withLocalContext:localContext];
            if ([arrNews count]) {
                [Product_Category MR_truncateAll];
            }
            NSLog(@"arr news count %d",[arrNews count]);
            for(NSInteger i=0;i<[arrNews count];i++){
                NSDictionary * dicEmp = (NSDictionary *)[arrNews objectAtIndex:i];
                strServerId = [dicEmp objectForKey:@"cateID"];
                //strLocalId = [dicEmp objectForKey:@"local_id"];
                strLocalId = @" ";
                //[self saveORupdateNewsWithServerId:strServerId andLocalId:strLocalId andDics:dicEmp];
                NSLog(@"cate name %@",[dicEmp objectForKey:@"name"]);
                
                NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"server_id == %@",strServerId];
                Product_Category * emp = [Product_Category findFirstWithPredicate:predicate inContext:localContext];
                if (emp != nil) {
                    
                    //emp.server_id = [dics objectForKey:@"newsID"];
                    //int ser_id = [[dicEmp objectForKey:@"id"] intValue];
                    //emp.server_id = [NSString stringWithFormat:@"%d",ser_id];
                    emp.server_id = [dicEmp objectForKey:@"cateID"];
                    emp.name = [dicEmp objectForKey:@"cateName"];
                    
                    emp.image_url = [dicEmp objectForKey:@"cateImage"];
                    emp.thumb = [dicEmp objectForKey:@"cateImage"];
                    
    //                emp.updated_time = [NSNumber numberWithInt:[[dicEmp objectForKey:@"updated_at"] intValue]];
    //                emp.is_active = [NSNumber numberWithInt:[[dicEmp objectForKey:@"is_active"] intValue]];
    //                emp.timetick = [NSNumber numberWithInt:[[dicEmp objectForKey:@"newsTimestamp"] intValue]];
                    [self updateProductCate:emp withLocalContext:localContext];
                }
                else{
                    Product_Category * objEmp2 = [Product_Category createEntity];
                    //int ser_id = [[dicEmp objectForKey:@"id"] intValue];
                    //objEmp2.server_id = [NSString stringWithFormat:@"%d",ser_id];
                    objEmp2.server_id = [dicEmp objectForKey:@"cateID"];
                    objEmp2.name = [dicEmp objectForKey:@"cateName"];
                    
                    objEmp2.image_url = [dicEmp objectForKey:@"cateImage"];
                    objEmp2.thumb = [dicEmp objectForKey:@"cateImage"];
                    
    //                objEmp2.updated_time = [NSNumber numberWithInt:[[dicEmp objectForKey:@"updated_at"] intValue]];
    //                objEmp2.is_active = [NSNumber numberWithInt:[[dicEmp objectForKey:@"is_active"] intValue]];
    //                objEmp2.timetick = [NSNumber numberWithInt:[[dicEmp objectForKey:@"newsTimestamp"] intValue]];
                    
                    [self insertProductCate:objEmp2 withLocalContext:localContext];
                }
            }
            
            
            NSArray * arrProducts = [myDict objectForKey:@"products"];
            if ([arrProducts count]) {
                [Products MR_truncateAll];
            }
            NSLog(@"arr products count %d",[arrNews count]);
            for(NSInteger i=0;i<[arrProducts count];i++){
                NSDictionary * dicEmp = (NSDictionary *)[arrProducts objectAtIndex:i];
                strServerId = [dicEmp objectForKey:@"productID"];
                //strLocalId = [dicEmp objectForKey:@"local_id"];
                strLocalId = @" ";
                //[self saveORupdateNewsWithServerId:strServerId andLocalId:strLocalId andDics:dicEmp];
                
                NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"server_id == %@",strServerId];
                Products * emp = [Products findFirstWithPredicate:predicate inContext:localContext];
                if (emp != nil) {
                    
                    emp.server_id = [dicEmp objectForKey:@"productID"];
                    emp.name = [dicEmp objectForKey:@"productName"];
                    emp.content = [dicEmp objectForKey:@"productContent"];
                    emp.image_url = [dicEmp objectForKey:@"productImage"];
                    emp.thumb = [dicEmp objectForKey:@"productThumbnail"];
                    emp.price_label = [dicEmp objectForKey:@"productPrice"];
                    NSString * strPrice = emp.price_label;
                    strPrice = [strPrice substringToIndex:0];
                    emp.price = [NSDecimalNumber decimalNumberWithString:strPrice];
                    emp.updated_time = [NSNumber numberWithInt:[[dicEmp objectForKey:@"updated_at"] intValue]];
                    emp.url = [dicEmp objectForKey:@"productLink"];
                    emp.is_active = [NSNumber numberWithInt:[[dicEmp objectForKey:@"is_active"] intValue]];
                    emp.timetick = [NSNumber numberWithInt:[[dicEmp objectForKey:@"newsTimestamp"] intValue]];
                    emp.cateID = [dicEmp objectForKey:@"cateID"];
                    emp.can_order = [NSNumber numberWithInt:[[dicEmp objectForKey:@"can_order"] intValue]];
                    [self updateProduct:emp withLocalContext:localContext];
                }
                else{
                    Products * objEmp2 = [Products createEntity];
                    //int ser_id = [[dicEmp objectForKey:@"productID"] intValue];
                    //objEmp2.server_id = [NSString stringWithFormat:@"%d",ser_id];
                    objEmp2.server_id = [dicEmp objectForKey:@"productID"];
                    objEmp2.name = [dicEmp objectForKey:@"productName"];
                    objEmp2.content = [dicEmp objectForKey:@"productContent"];
                    objEmp2.image_url = [dicEmp objectForKey:@"productImage"];
                    objEmp2.thumb = [dicEmp objectForKey:@"productThumbnail"];
                    objEmp2.price_label = [dicEmp objectForKey:@"productPrice"];
                    NSString * strPrice = objEmp2.price_label;
                    strPrice = [strPrice substringToIndex:0];
                    objEmp2.price = [NSDecimalNumber decimalNumberWithString:strPrice];
                    objEmp2.updated_time = [NSNumber numberWithInt:[[dicEmp objectForKey:@"updated_at"] intValue]];
                    objEmp2.url = [dicEmp objectForKey:@"productLink"];
                    objEmp2.is_active = [NSNumber numberWithInt:[[dicEmp objectForKey:@"is_active"] intValue]];
                    objEmp2.timetick = [NSNumber numberWithInt:[[dicEmp objectForKey:@"newsTimestamp"] intValue]];
                    objEmp2.can_order = [NSNumber numberWithInt:[[dicEmp objectForKey:@"can_order"] intValue]];
    
                    objEmp2.cateID  = [dicEmp objectForKey:@"cateID"];
                    [self insertProducts:objEmp2 withLocalContext:localContext];
                }
            }
             [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshProductView" object:nil];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [SVProgressHUD dismiss];
            NSLog(@"Error %@",error);
            //[SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
            //[SVProgressHUD dismiss];
        }];
    }
    
}

- (void) updateProductCate:(Product_Category *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    
    // Create a new Person in the current thread context
    //obj                          = [Employee MR_createInContext:localContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"server_id ==[c] %@ ", obj.server_id];
    Product_Category * employeeFounded = [Product_Category MR_findFirstWithPredicate:predicate inContext:localContext];
    if (employeeFounded) {
        /*[localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
         Employee * objEmp = [Employee MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
         NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
         }];*/
        employeeFounded = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            Product_Category * objEmp = [Product_Category MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
            NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
        }];
    }
}

- (void) updateProductCateWithLocalID:(Product_Category *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    
    // Create a new Person in the current thread context
    //obj                          = [Employee MR_createInContext:localContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"local_id ==[c] %@ ", obj.local_id];
    Product_Category * employeeFounded = [Product_Category MR_findFirstWithPredicate:predicate inContext:localContext];
    if (employeeFounded) {
        /*[localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
         Employee * objEmp = [Employee MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
         NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
         }];*/
        employeeFounded = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            Product_Category * objEmp = [Product_Category MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
            NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
        }];
    }
}

- (void) insertProductCate:(Product_Category *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    // Get the local context
    // Create a new Person in the current thread context
    //Employee *person                          = [Employee MR_createInContext:localContext];
    //person = obj;
    [obj MR_inContext:localContext];
    
    // Save the modification in the local context
    [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        NSLog(@"Product Category saved");
    }];
}

- (void) updateProduct:(Products *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    
    // Create a new Person in the current thread context
    //obj                          = [Employee MR_createInContext:localContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"server_id ==[c] %@ ", obj.server_id];
    Products * employeeFounded = [Products MR_findFirstWithPredicate:predicate inContext:localContext];
    if (employeeFounded) {
        /*[localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
         Employee * objEmp = [Employee MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
         NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
         }];*/
        employeeFounded = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            Products * objEmp = [Products MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
            NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
        }];
    }
}

- (void) updateProductsWithLocalID:(Products *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    
    // Create a new Person in the current thread context
    //obj                          = [Employee MR_createInContext:localContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"local_id ==[c] %@ ", obj.local_id];
    Products * employeeFounded = [Products MR_findFirstWithPredicate:predicate inContext:localContext];
    if (employeeFounded) {
        /*[localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
         Employee * objEmp = [Employee MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
         NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
         }];*/
        employeeFounded = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            Products * objEmp = [Products MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
            NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
        }];
    }
}

- (void) insertProducts:(Products *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    // Get the local context
    // Create a new Person in the current thread context
    //Employee *person                          = [Employee MR_createInContext:localContext];
    //person = obj;
    [obj MR_inContext:localContext];
    
    // Save the modification in the local context
    [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        NSLog(@"Products saved");
    }];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
