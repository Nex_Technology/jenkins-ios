//
//  StoresViewController.m
//  IACTAC
//
//  Created by Zayar on 10/29/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "StoresViewController.h"
#import "Stores.h"
#import "SWStoreTableViewCell.h"
#import "DisplayMap.h"
#import "UITableView+ZGParallelView.h"
#import "Utility.h"
#import "TSMiniWebBrowser.h"
#import "AppDelegate.h"

@interface StoresViewController ()
{
        NSArray * arr;
        IBOutlet UITableView * tbl;
        IBOutlet UIScrollView * scrollView;
}
@property (nonatomic, strong) IBOutlet MKMapView * mapView;
@end

@implementation StoresViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([tbl respondsToSelector:@selector(separatorInset)]) {
        [tbl setSeparatorInset:UIEdgeInsetsZero];
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [self.mapView setFrame:CGRectMake(0, 0, self.mapView.frame.size.width, self.mapView.frame.size.height+44+44+20+6)];
    }
    else{
        CGRect newRect = scrollView.frame;
        CGRect newTblRect = tbl.frame;
        if ([Utility isGreaterOREqualOSVersion:@"7.0"] ) {
            [self.mapView setFrame:CGRectMake(0, 0, self.mapView.frame.size.width, self.mapView.frame.size.height+44+44+20+6)];
        }
        else{
            [self.mapView setFrame:CGRectMake(0, 104, self.mapView.frame.size.width, self.mapView.frame.size.height+44+44+20+6)];
            newTblRect.origin.y += 88;
            [tbl setFrame:newTblRect];
        }
    }
    
    CALayer * bottomBorder = [CALayer layer];
    bottomBorder.borderColor = [[UIColor colorWithHexString:@"DADADA"] CGColor];
    bottomBorder.borderWidth = 1.0f;
    bottomBorder.frame = CGRectMake(-1, 1, CGRectGetWidth(self.mapView.frame), CGRectGetHeight(self.mapView.frame));
    [self.mapView.layer addSublayer:bottomBorder];

    tbl.backgroundColor = [UIColor colorWithHexString: @"f0f0f0"];
    //tbl.scrollEnabled = NO;
    
    UIBarButtonItem * btnOrder = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_refresh"] style:UIBarButtonItemStylePlain target:self action:@selector(refreshTheData:)];
    self.navigationItem.rightBarButtonItem = btnOrder;

}

- (void)refreshTheData:(UIBarButtonItem *)sender{
    AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    [SVProgressHUD show];
    [delegate syncProducts];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
        if (scrollView == tbl) {
            [tbl updateParallelViewWithOffset:scrollView.contentOffset];
    }
}
    
- (void) viewWillAppear:(BOOL)animated{
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
    //arr = [Stores MR_findAllInContext:localContext];
    arr  = [Stores MR_findAllSortedBy:@"order" ascending:YES inContext:localContext];
    NSLog(@"stores arr count %d",[arr count]);
    
    [scrollView setContentSize:CGSizeMake(320, self.mapView.frame.size.height + tbl.contentSize.height)];
}
    
- (void)viewDidAppear:(BOOL)animated{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (void)removeAllAnnotations {
    
    NSMutableArray *annotationsToRemove = [NSMutableArray arrayWithCapacity:[self.mapView.annotations count]];
    NSLog(@"self mapview count %d",[self.mapView.annotations count]);
    
    for (int i = 0; i < [self.mapView.annotations count]; i++) {
        NSLog(@"removepin......");
        //if ([[self.mapView.annotations objectAtIndex:i] isKindOfClass:[DisplayMap class]]) {
        NSLog(@"removepin2......");
        [annotationsToRemove addObject:[self.mapView.annotations objectAtIndex:i]];
        //}
    }
    
    [self.mapView removeAnnotations:annotationsToRemove];
}
    
- (void)setUserSelectedLocation:(float)lat andLong:(float)lon{
        [self removeAllAnnotations];
        
        if (lat != 0.0 && lon != 0.0) {
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
            CLLocationCoordinate2D touchMapCoordinate = location.coordinate;
            
            DisplayMap *ann = [[DisplayMap alloc] init];
            //ann.canShowCallout=YES;
            //ann.animatesDrop=YES;
            ann.coordinate = touchMapCoordinate;
            [self.mapView addAnnotation:ann];
            /*MKMapRect zoomRect = MKMapRectMake(0, 0, 0.002, 0.002);
            for (id <MKAnnotation> annotation in self.mapView.annotations)
            {
                MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
                MKMapRect pointRect = MKMapRectMake(annotationPoint.x , annotationPoint.y , 0.002, 0.002);// 0.1, 0.1
                zoomRect = MKMapRectUnion(zoomRect, pointRect);
            }
            [self.mapView setVisibleMapRect:zoomRect animated:YES];*/
            //[self.mapView setVisibleMapRect:zoomRect edgePadding:UIEdgeInsetsMake(-100, -50, -50, -50) animated:YES];
            MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(touchMapCoordinate, 1000, 1000);
            MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
            [self.mapView setRegion:adjustedRegion animated:YES];
            //[mapView setCenterCoordinate:myCoord zoomLevel:13 animated:YES];
        }
}
    
#pragma uitableview delegate and datasources
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        static NSString *cellIdentifier = @"Cell";
        
        SWStoreTableViewCell *cell = (SWStoreTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        //if (cell == nil) {
        NSMutableArray *leftUtilityButtons = [NSMutableArray new];
        NSMutableArray *rightUtilityButtons = [NSMutableArray new];
        
        //[rightUtilityButtons addUtilityButtonWithColor:[UIColor colorWithHexString:@"33d77c"] title:@"Call"];
        //[rightUtilityButtons addUtilityButtonWithColor:[UIColor colorWithHexString:@"39579A"] title:@"Visit"];
        
        if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
            cell = [[SWStoreTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier height:([SWStoreTableViewCell heightForCellWithPost:[arr objectAtIndex:indexPath.row]]+ 40) leftUtilityButtons:leftUtilityButtons rightUtilityButtons:rightUtilityButtons];
        }
        else cell = [[SWStoreTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier height:([SWStoreTableViewCell heightForCellWithPost:[arr objectAtIndex:indexPath.row]]+ 40) leftUtilityButtons:leftUtilityButtons rightUtilityButtons:rightUtilityButtons];
        
        // }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        Stores * obj = [arr objectAtIndex:[indexPath row]];
        
        cell.tag = indexPath.row;
        cell.delegate = self;
        
        [cell loadTheViewWith:obj];

        //[cell reloadTheScrollViewHeight:([ScheduleCell heightForCellWithPost:[arrSchedules objectAtIndex:indexPath.row]]+ 81)];
        if (indexPath.row == 0) {
            [self loadTheMapWith:obj];
        }
    
        return cell;
}
    
- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        return ([SWStoreTableViewCell heightForCellWithPost:[arr objectAtIndex:indexPath.row]] + 40);
    }
    else return ([SWStoreTableViewCell heightForCellWithPost:[arr objectAtIndex:indexPath.row]]+40);
}
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*if (newsDetailViewController == nil) {
        newsDetailViewController = [[NewsDetailViewController alloc] init];
    }
    News * obj = [arr objectAtIndex:indexPath.row];
    [newsDetailViewController loadTheViewWithNews:obj];
    [self.navigationController pushViewController:newsDetailViewController animated:YES];
    [self.navigationItem.backBarButtonItem setTintColor:[UIColor redColor]];*/
}
    
- (void)swippableTableViewCell:(SWStoreTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
        switch (index) {
            case 0:
            NSLog(@"left button 0 was pressed");
            break;
            case 1:
            NSLog(@"left button 1 was pressed");
            break;
            case 2:
            NSLog(@"left button 2 was pressed");
            break;
            case 3:
            NSLog(@"left btton 3 was pressed");
            default:
            break;
        }
    }
    
- (void)swippableTableViewCell:(SWStoreTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
        {
            //NSLog(@"More button was pressed");
           
            NSLog(@"here is call!!");
            Stores * obj = [arr objectAtIndex:cell.tag];
            NSString * number = [NSString stringWithFormat:@"tel://%@", [obj.phone stringByReplacingOccurrencesOfString:@"-" withString:@""]];
            NSLog(@"Calling %@", number);
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: number]];
            break;
        }
        case 1:
        {
            // Delete button was pressed
            //NSIndexPath *cellIndexPath = [tbl indexPathForCell:cell];
            Stores * obj = [arr objectAtIndex:cell.tag];
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: obj.fb_link]];
            break;
        }
        default:
        break;
    }
}

- (void)onSWCellCall:(SWStoreTableViewCell *)cell andButton:(UIButton *)sender{
    Stores * obj = [arr objectAtIndex:cell.tag];
    NSString * number = [NSString stringWithFormat:@"tel://%@", [obj.phone stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    NSLog(@"Calling %@", number);
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: number]];
}

- (void)onSWCellFb:(SWStoreTableViewCell *)cell andButton:(UIButton *)sender{
    NSLog(@"on fbbb!");
    Stores * obj = [arr objectAtIndex:cell.tag];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: [NSString stringWithFormat:@"fb://profile/%@",obj.fb_id]]];
    //TSMiniWebBrowser *webBrowser = [[TSMiniWebBrowser alloc] initWithUrl:[NSURL URLWithString:obj.fb_link]];
    //[self.navigationController pushViewController:webBrowser animated:YES];
}

- (void)onSWCellMap:(SWStoreTableViewCell *)cell andButton:(UIButton *)sender{
    NSLog(@"on mappp!");
    Stores * obj = [arr objectAtIndex:cell.tag];
    NSString *title = obj.title;
    float latitude = [obj.lat floatValue];
    float longitude = [obj.longtitude floatValue];
    //int zoom = 13;
    
    CLLocationCoordinate2D rdOfficeLocation = CLLocationCoordinate2DMake(latitude,longitude);
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:rdOfficeLocation addressDictionary:nil];
    MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
    item.name = title;
    [item openInMapsWithLaunchOptions:nil];
    /*NSString *stringURL = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@@%1.6f,%1.6f&z=%d", title, latitude, longitude, zoom];
    NSURL *url = [NSURL URLWithString:stringURL];
    [[UIApplication sharedApplication] openURL:url];*/
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
    
- (void)didSelectedTheCell:(SWStoreTableViewCell *)cell{
    
        /*ObjSchedule * obj = [arrSchedules objectAtIndex:cell.tag];
        ScheduleDetailViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ScheduleDetail"];
        viewController.objSchedule = obj;
        [self.navigationController pushViewController:viewController animated:YES];*/
    NSLog(@"cell selected!!");
    Stores * obj = [arr objectAtIndex:cell.tag];
    [self loadTheMapWith:obj];
}
    
- (void) loadTheMapWith:(Stores *)obj{
    [self setUserSelectedLocation:[obj.lat floatValue] andLong:[obj.longtitude floatValue]];
}
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return [arr count];
}
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
    {
#warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1;
    }

@end
