//
//  SWTableViewCell.h
//  SWTableViewCell
//
//  Created by Chris Wendel on 9/10/13.
//  Copyright (c) 2013 Chris Wendel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stores.h"
@class SWStoreTableViewCell;

@protocol SWTableViewCellDelegate <NSObject>

- (void)swippableTableViewCell:(SWStoreTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index;
- (void)swippableTableViewCell:(SWStoreTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index;
- (void)didSelectedTheCell:(SWStoreTableViewCell *)cell;
- (void)onSWCellCall:(SWStoreTableViewCell *)cell andButton:(UIButton *)sender;
- (void)onSWCellFb:(SWStoreTableViewCell *)cell andButton:(UIButton *)sender;
- (void)onSWCellMap:(SWStoreTableViewCell *)cell andButton:(UIButton *)sender;
@end

@interface SWStoreTableViewCell : UITableViewCell
{
    /**** for Schedule cell ****/
    
    UILabel * lblName;
    UILabel * lblLocation;
    UIImageView * imageLocationView;
    UIImageView * imgViewMapIcon;
    UIButton * btnCall;
    UIButton * btnFbVisit;
    UIButton * btnMap;
    /**** end for Schedule cell ****/
}

@property (nonatomic, strong) NSArray *leftUtilityButtons;
@property (nonatomic, strong) NSArray *rightUtilityButtons;
@property (nonatomic) id <SWTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *mainView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier height:(CGFloat)height leftUtilityButtons:(NSArray *)leftUtilityButtons rightUtilityButtons:(NSArray *)rightUtilityButtons;

- (void)wayToOriginalScrollView;

/**** for Schedule cell ****/
- (void)setUI;
- (void)loadTheViewWith:(Stores *)obj;
+ (CGFloat)heightForCellWithPost:(Stores *)obj;
- (CGFloat)heightForCellWithPureHeight:(Stores *)obj;
- (void) reloadTheScrollViewHeight:(float)cellHeight;
- (void) checkAndDisableButtons:(Stores *)obj;
/**** end for Schedule cell ****/
@end

@interface NSMutableArray (SWUtilityButtons)

- (void)addUtilityButtonWithColor:(UIColor *)color title:(NSString *)title;
- (void)addUtilityButtonWithColor:(UIColor *)color icon:(UIImage *)icon;
- (void)addUtilityButtonWithColor:(UIColor *)color icon:(UIImage *)icon andTag:(int)tag;

@end
