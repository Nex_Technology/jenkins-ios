//
//  Products.m
//  fyre
//
//  Created by Zayar on 3/7/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Products.h"


@implementation Products

@dynamic cateID;
@dynamic content;
@dynamic date;
@dynamic detail;
@dynamic image_url;
@dynamic is_active;
@dynamic isWeb;
@dynamic local_id;
@dynamic name;
@dynamic price;
@dynamic price_label;
@dynamic server_id;
@dynamic thumb;
@dynamic timetick;
@dynamic updated_time;
@dynamic url;
@dynamic can_order;

@end
