//
//  NewsCell.m
//  IACTAC
//
//  Created by Zayar on 10/29/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "NewsCell.h"
#import "News.h"
#import "UIImageView+AFNetworking.h"
#import "Utility.h"
@implementation NewsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setUpViews{
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 65, 88)];
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    //imgView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth);
    [self addSubview:imgView];
    
    lblName = [[UILabel alloc] initWithFrame:CGRectMake(105, 10, 200, 68)];
    lblName.textColor = [UIColor colorWithHexString:@"6C6C6C"];
    lblName.numberOfLines = 2;
    lblName.lineBreakMode = NSLineBreakByWordWrapping;
    lblName.font = [UIFont fontWithName:@"AvenirNext-Medium" size:16];
    [self addSubview:lblName];
}

- (void) loadTheView:(News *) obj{
    NSLog(@"image url %@",obj.image_url);
    [imgView setImageWithURL:[NSURL URLWithString:obj.image_url] placeholderImage:nil];
    
    lblName.text = obj.title;
}

@end
