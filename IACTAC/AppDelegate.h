//
//  AppDelegate.h
//  IACTAC
//
//  Created by Zayar on 10/29/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"
#import "News.h"
#import "Config.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    Facebook * facebook;
    NSMutableDictionary *userPermissions;
    NSArray *permissions;
    Config * conf;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Facebook *facebook;
@property (nonatomic, retain) NSMutableDictionary *userPermissions;
@property (nonatomic, retain) NSArray *permissions;
- (void) syncNews;
- (void) syncStores;
- (void) syncProducts;
- (void)fbShare:(News *)obj;
@end
