//
//  Config_Tabs.m
//  fyre
//
//  Created by Zayar on 3/7/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Config_Tabs.h"


@implementation Config_Tabs

@dynamic api_url;
@dynamic app_id;
@dynamic icon;
@dynamic order_id;
@dynamic title;
@dynamic type;
@dynamic tab_id;

@end
