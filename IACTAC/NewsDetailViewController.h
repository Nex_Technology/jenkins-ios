//
//  NewsDetailViewController.h
//  IACTAC
//
//  Created by Zayar on 10/30/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"
#import "NXViewController.h"
@interface NewsDetailViewController : NXViewController

- (void)loadTheViewWithNews:(News *) obj;
@end
