//
//  ContactCell.m
//  IACTAC
//
//  Created by Zayar on 11/4/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell
@synthesize txtValue;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setUpViews{
    if(!lblName)
    lblName = [[UILabel alloc] initWithFrame:CGRectMake(20, 8, 100, 30)];
    
    lblName.textColor = [UIColor colorWithHexString:@"6C6C6C"];
    lblName.font = [UIFont fontWithName:@"AvenirNext-Medium" size:16];
    lblName.text = @"";
    lblName.backgroundColor = [UIColor clearColor];
    [self addSubview:lblName];
    
    if(!self.txtValue)
    self.txtValue = [[UITextField alloc] initWithFrame:CGRectMake(lblName.frame.origin.x+lblName.frame.size.width+3, 8, 190, 30)];
    
    self.txtValue.textAlignment = NSTextAlignmentRight;
    self.txtValue.font = [UIFont fontWithName:@"AvenirNext-Regular" size:16];
    self.txtValue.textColor = [UIColor colorWithHexString:@"3C3C3C"];
    self.txtValue.placeholder = @"";
    [self addSubview:txtValue];
}

- (void) loadTextWith:(NSString *)strName andPlaceHolder:(NSString *)strPlace{
    
    lblName.text = strName;
    txtValue.placeholder = strPlace;
    txtValue.text = @"";
}

- (NSString *) getTheValueOfTheValue{
    return txtValue.text;
}

@end
