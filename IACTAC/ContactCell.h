//
//  ContactCell.h
//  IACTAC
//
//  Created by Zayar on 11/4/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell
{
    UILabel * lblName;
    UITextField * txtValue;
}
- (NSString *) getTheValueOfTheValue;
- (void) setUpViews;
- (void) loadTextWith:(NSString *)strName andPlaceHolder:(NSString *)strPlace;
@property (nonatomic,retain) UITextField * txtValue;
@end
