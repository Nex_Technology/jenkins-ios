//
//  SWTableViewCell.m
//  SWTableViewCell
//
//  Created by Chris Wendel on 9/10/13.
//  Copyright (c) 2013 Chris Wendel. All rights reserved.
//

#import "SWStoreTableViewCell.h"
#import "UIColor+Expanded.h"
#import <QuartzCore/QuartzCore.h>
#import "Utility.h"
#define kUtilityButtonsWidthMax 260
#define kUtilityButtonWidthDefault 120

static NSString * const kTableViewCellContentView = @"UITableViewCellContentView";

typedef enum {
    kCellStateCenter,
    kCellStateLeft,
    kCellStateRight
} SWCellState;

#pragma mark - SWUtilityButtonView

@interface SWUtilityButtonView : UIView

@property (nonatomic, strong) NSArray *utilityButtons;
@property (nonatomic) CGFloat utilityButtonWidth;
@property (nonatomic, weak) SWStoreTableViewCell *parentCell;
@property (nonatomic) SEL utilityButtonSelector;

- (id)initWithUtilityButtons:(NSArray *)utilityButtons parentCell:(SWStoreTableViewCell *)parentCell utilityButtonSelector:(SEL)utilityButtonSelector;

- (id)initWithFrame:(CGRect)frame utilityButtons:(NSArray *)utilityButtons parentCell:(SWStoreTableViewCell *)parentCell utilityButtonSelector:(SEL)utilityButtonSelector;

@end

@implementation SWUtilityButtonView

#pragma mark - SWUtilityButonView initializers

- (id)initWithUtilityButtons:(NSArray *)utilityButtons parentCell:(SWStoreTableViewCell *)parentCell utilityButtonSelector:(SEL)utilityButtonSelector {
    self = [super init];
    
    if (self) {
        self.utilityButtons = utilityButtons;
        self.utilityButtonWidth = [self calculateUtilityButtonWidth];
        self.parentCell = parentCell;
        self.utilityButtonSelector = utilityButtonSelector; // eh.
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame utilityButtons:(NSArray *)utilityButtons parentCell:(SWStoreTableViewCell *)parentCell utilityButtonSelector:(SEL)utilityButtonSelector {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.utilityButtons = utilityButtons;
        self.utilityButtonWidth = [self calculateUtilityButtonWidth];
        self.parentCell = parentCell;
        self.utilityButtonSelector = utilityButtonSelector; // eh.
    }
    
    return self;
}

#pragma mark Populating utility buttons

- (CGFloat)calculateUtilityButtonWidth {
    CGFloat buttonWidth = kUtilityButtonWidthDefault;
    if (buttonWidth * _utilityButtons.count > kUtilityButtonsWidthMax) {
        CGFloat buffer = (buttonWidth * _utilityButtons.count) - kUtilityButtonsWidthMax;
        buttonWidth -= (buffer / _utilityButtons.count);
    }
    return buttonWidth;
}

- (CGFloat)utilityButtonsWidth {
    return (_utilityButtons.count * _utilityButtonWidth);
}

- (void)populateUtilityButtons {
    NSUInteger utilityButtonsCounter = 0;
    for (UIButton *utilityButton in _utilityButtons) {
        CGFloat utilityButtonXCord = 0;
        if (utilityButtonsCounter >= 1) utilityButtonXCord = _utilityButtonWidth * utilityButtonsCounter;
        [utilityButton setFrame:CGRectMake(utilityButtonXCord, 0, _utilityButtonWidth, CGRectGetHeight(self.bounds))];
        [utilityButton setTag:utilityButtonsCounter];
        [utilityButton addTarget:self.parentCell action:self.utilityButtonSelector forControlEvents:UIControlEventTouchDown];
        [self addSubview: utilityButton];
        utilityButtonsCounter++;
    }
}

@end

@interface SWStoreTableViewCell () <UIScrollViewDelegate> {
    SWCellState _cellState; // The state of the cell within the scroll view, can be left, right or middle
    UIScrollView *cellScrollView;
}

// Scroll view to be added to UITableViewCell
@property (nonatomic, weak) UIScrollView *cellScrollView;

// The cell's height
@property (nonatomic) CGFloat height;

// Views that live in the scroll view
@property (nonatomic, weak) UIView *scrollViewContentView;
@property (nonatomic, strong) SWUtilityButtonView *scrollViewButtonViewLeft;
@property (nonatomic, strong) SWUtilityButtonView *scrollViewButtonViewRight;

@end

@implementation SWStoreTableViewCell
#pragma mark ScheduleCell class
- (void)setUI{

    lblName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
    lblName.font = [UIFont fontWithName:@"AvenirNext-Medium" size:14.0f];
    lblName.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    lblName.textColor = [UIColor colorWithHexString:@"6C6C6C"];

    CALayer * bottomBorder = [CALayer layer];
    bottomBorder.borderColor = [[UIColor colorWithHexString:@"DADADA"] CGColor];
    bottomBorder.borderWidth = 1.0f;
    bottomBorder.frame = CGRectMake(0, -1, CGRectGetWidth(lblName.frame), CGRectGetHeight(lblName.frame));

    [lblName.layer addSublayer:bottomBorder];
    
    lblName.numberOfLines = 1;
    [self addSubview:lblName];
    
    lblLocation = [[UILabel alloc] initWithFrame:CGRectMake(20, 35, 170, 70)];
    lblLocation.font = [UIFont fontWithName:@"Avenir Next Medium" size:13.0f];
    lblLocation.backgroundColor = [UIColor colorWithHexString:@"fbfbfc"];
    lblLocation.textColor = [UIColor colorWithHexString:@"535353"];
    lblLocation.numberOfLines = 0;
    self.backgroundColor = [UIColor colorWithHexString:@"fbfbfc"];
    [self addSubview:lblLocation];
    
    btnCall = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCall setImage:[UIImage imageNamed:@"call"] forState:normal];
    [btnCall setFrame:CGRectMake(lblLocation.frame.origin.x+lblLocation.frame.size.width+10, lblName.frame.origin.y+lblName.frame.size.height+20, 30, 30)];
    [btnCall addTarget:self action:@selector(onCall:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnCall];
    
    btnFbVisit = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnFbVisit setImage:[UIImage imageNamed:@"485-facebook"] forState:normal];
    [btnFbVisit setFrame:CGRectMake(btnCall.frame.origin.x+btnCall.frame.size.width + 10, lblName.frame.origin.y+lblName.frame.size.height+20, 30, 30)];
    [btnFbVisit addTarget:self action:@selector(onFb:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnFbVisit];
    
    btnMap = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnMap setImage:[UIImage imageNamed:@"btn_map"] forState:normal];
    [btnMap setFrame:CGRectMake(btnFbVisit.frame.origin.x+btnFbVisit.frame.size.width + 10, lblName.frame.origin.y+lblName.frame.size.height+20, 30, 30)];
    [btnMap addTarget:self action:@selector(onMap:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnMap];
    
    //imgViewMapIcon = [[UIImageView alloc] initWithFrame:CGRectMake(280, 40, 50, 50)];
    [self setBackgroundColor:[UIColor colorWithHexString:@"fbfbfc"]];
}

- (void)onCall:(UIButton *)sender{
    [_delegate onSWCellCall:self andButton:sender];
}

- (void)onFb:(UIButton *)sender{
    [_delegate onSWCellFb:self andButton:sender];
}

- (void)onMap:(UIButton *)sender{
    [_delegate onSWCellMap:self andButton:sender];
}

- (void)loadTheViewWith:(Stores *)obj{
    CGRect detailTextLabelFrame = lblLocation.frame;
    detailTextLabelFrame.size.height = [self heightForCellWithPureHeight:obj];
    lblLocation.frame = detailTextLabelFrame;
    
    lblName.text = [NSString stringWithFormat:@"   %10s",[obj.title UTF8String]];
    lblLocation.text = [NSString stringWithFormat:@"%10s",[obj.location UTF8String]];
    [self setNeedsLayout];
    
    [self checkAndDisableButtons:obj];
}

+ (CGFloat)heightForCellWithPost:(Stores *)obj {
    CGSize sizeToFit = [obj.location sizeWithFont:[UIFont fontWithName:@"Avenir Next Medium" size:13.0f] constrainedToSize:CGSizeMake(170, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    NSLog(@"size to fix %f",sizeToFit.height + 81.0f);
    return fmaxf(70.0f, sizeToFit.height+ 5);
}

- (CGFloat)heightForCellWithPureHeight:(Stores *)obj {
    CGSize sizeToFit = [obj.location sizeWithFont:[UIFont fontWithName:@"Avenir Next Medium" size:13.0f] constrainedToSize:CGSizeMake(170, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    
    return fmaxf(70.0f, sizeToFit.height+5);
}

- (void) onLocation:(UIButton *)sendder{
    NSLog(@"location!!!");
}

#pragma mark Initializers

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier
    height:(CGFloat)height
    leftUtilityButtons:(NSArray *)leftUtilityButtons
    rightUtilityButtons:(NSArray *)rightUtilityButtons {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.rightUtilityButtons = rightUtilityButtons;
        self.leftUtilityButtons = leftUtilityButtons;
        self.height = height;
        [self initializer];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self initializer];
    }
    
    return self;
}

- (id)init {
    self = [super init];
    
    if (self) {
        [self initializer];
    }
    
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self initializer];
    }
    
    return self;
}

- (void)initializer {
    self.mainView.layer.cornerRadius = 10;
    self.mainView.layer.masksToBounds = YES;
    self.mainView = self;
    // Set up scroll view that will host our cell content
    if (cellScrollView == nil) {
        cellScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), _height)];
    }
    cellScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.bounds) + [self utilityButtonsPadding], _height);
    cellScrollView.contentOffset = [self scrollViewContentOffset];
    cellScrollView.delegate = self;
    cellScrollView.showsHorizontalScrollIndicator = NO;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollTapGesture:)];
    tapGesture.numberOfTapsRequired = 1;
    [cellScrollView addGestureRecognizer:tapGesture];
    
    self.cellScrollView = cellScrollView;
    
    // Set up the views that will hold the utility buttons
    SWUtilityButtonView *scrollViewButtonViewLeft = [[SWUtilityButtonView alloc] initWithUtilityButtons:_leftUtilityButtons parentCell:self utilityButtonSelector:@selector(leftUtilityButtonHandler:)];
    [scrollViewButtonViewLeft setFrame:CGRectMake([self leftUtilityButtonsWidth], 0, [self leftUtilityButtonsWidth], _height)];
    self.scrollViewButtonViewLeft = scrollViewButtonViewLeft;
    [self.cellScrollView addSubview:scrollViewButtonViewLeft];
    
    SWUtilityButtonView *scrollViewButtonViewRight = [[SWUtilityButtonView alloc] initWithUtilityButtons:_rightUtilityButtons parentCell:self utilityButtonSelector:@selector(rightUtilityButtonHandler:)];
    [scrollViewButtonViewRight setFrame:CGRectMake(CGRectGetWidth(self.bounds), 0, [self rightUtilityButtonsWidth], _height)];
    self.scrollViewButtonViewRight = scrollViewButtonViewRight;
    [self.cellScrollView addSubview:scrollViewButtonViewRight];
    
    // Populate the button views with utility buttons
    [scrollViewButtonViewLeft populateUtilityButtons];
    [scrollViewButtonViewRight populateUtilityButtons];
    
    
    
    /**** for Schedule cell ****/
    /*lblVerticalStrip = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 3, 137)];
    lblVerticalStrip.text = @"";
    
    [self addSubview:lblVerticalStrip];
    
    lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(17, 12, 206, 50)];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont fontWithName:@"Avenir Next Medium" size:16.0f];
    lblTitle.textColor = [UIColor colorWithRed:0/255.f green:91/255.f blue:113/255.f alpha:1];
    lblTitle.numberOfLines = 0;
    
    [self addSubview:lblTitle];
    
    lblTime = [[UILabel alloc]initWithFrame:CGRectMake(250, 57, 80, 23)];
    lblTime.backgroundColor = [UIColor clearColor];
    //lblTime.font = [UIFont boldSystemFontOfSize:15];
    lblTime.font = [UIFont fontWithName:@"AvenirNext-Bold" size:15.0f];
    lblTime.textColor = [UIColor colorWithRed:221/255.f green:126/255.f blue:55/255.f alpha:1];
    [self addSubview:lblTime];
    
    lblSpeaker = [[UILabel alloc]initWithFrame:CGRectMake(17, (lblTitle.frame.origin.y + lblTitle.frame.size.height) + 3, 206, 23)];
    lblSpeaker.backgroundColor = [UIColor clearColor];
    lblSpeaker.font = [UIFont fontWithName:@"Avenir Next Medium" size:15.0f];
    lblSpeaker.textColor = [UIColor colorWithRed:97/255.f green:87/255.f blue:87/255.f alpha:1];
    [self addSubview:lblSpeaker];
    
    btnLocation = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLocation.frame = CGRectMake(17, (lblSpeaker.frame.origin.y + lblSpeaker.frame.size.height) + 3, 150, 30);
    //btnLocation.titleLabel = lblSpeaker;
    btnLocation.titleLabel.font = [UIFont fontWithName:@"Avenir Next Medium" size:15.0f];
    btnLocation.titleLabel.textColor = [UIColor colorWithRed:221/255.f green:126/255.f blue:55/255.f alpha:1];
    [btnLocation setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [btnLocation addTarget:self action:@selector(onLocation:) forControlEvents:UIControlEventTouchUpInside];
    btnLocation.tag = self.tag;
    //btnLocation
    
    [self addSubview:btnLocation];
    /**** end for Schedule cell ****/
    [self setUI];
    
    
    // Create the content view that will live in our scroll view
    UIView *scrollViewContentView = [[UIView alloc] initWithFrame:CGRectMake([self leftUtilityButtonsWidth], 0, CGRectGetWidth(self.bounds), _height)];
    scrollViewContentView.backgroundColor = [UIColor colorWithHexString:@"fbfbfc"];
    [self.cellScrollView addSubview:scrollViewContentView];
    self.scrollViewContentView = scrollViewContentView;
    
    // Add the cell scroll view to the cell
    UIView *contentViewParent = self;
    if (![NSStringFromClass([[self.subviews objectAtIndex:0] class]) isEqualToString:kTableViewCellContentView]) {
        // iOS 7
        contentViewParent = [self.subviews objectAtIndex:0];
    }
    NSArray *cellSubviews = [contentViewParent subviews];
    [self insertSubview:cellScrollView atIndex:0];
    for (UIView *subview in cellSubviews) {
        [self.scrollViewContentView addSubview:subview];
    }
}

- (void) checkAndDisableButtons:(Stores *)obj{
    //btnMap.enabled = YES;
    btnCall.enabled = YES;
    btnFbVisit.enabled = YES;
    if ([Utility stringIsEmpty:obj.fb_link shouldCleanWhiteSpace:YES]) {
        btnFbVisit.enabled = NO;
    }
    
    if ([Utility stringIsEmpty:obj.phone shouldCleanWhiteSpace:YES]) {
        btnCall.enabled = NO;
    }
}

-(void) scrollTapGesture:(UIGestureRecognizer *) sender {
    [_delegate didSelectedTheCell:self];
}

- (void) reloadTheScrollViewHeight:(float)cellHeight{
    if (cellScrollView != nil) {
        _height = cellHeight;
        cellScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), _height)];
        cellScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.bounds) + [self utilityButtonsPadding], _height);
    }
}

#pragma mark - Utility buttons handling

- (void)rightUtilityButtonHandler:(id)sender {
    UIButton *utilityButton = (UIButton *)sender;
    NSInteger utilityButtonTag = [utilityButton tag];
    [_delegate swippableTableViewCell:self didTriggerRightUtilityButtonWithIndex:utilityButtonTag];
}

- (void)leftUtilityButtonHandler:(id)sender {
    UIButton *utilityButton = (UIButton *)sender;
    NSInteger utilityButtonTag = [utilityButton tag];
    [_delegate swippableTableViewCell:self didTriggerLeftUtilityButtonWithIndex:utilityButtonTag];
}


#pragma mark - Overriden methods
- (void)layoutSubviews {
    [super layoutSubviews];
    NSLog(@"row height %f",_height);
    self.cellScrollView.frame = CGRectMake(0, 0, CGRectGetWidth(self.bounds), _height);
    self.cellScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.bounds) + [self utilityButtonsPadding], _height);
    self.cellScrollView.contentOffset = CGPointMake([self leftUtilityButtonsWidth], 0);
    self.scrollViewButtonViewLeft.frame = CGRectMake([self leftUtilityButtonsWidth], 0, [self leftUtilityButtonsWidth], _height);
    self.scrollViewButtonViewRight.frame = CGRectMake(CGRectGetWidth(self.bounds), 0, [self rightUtilityButtonsWidth], _height);
    self.scrollViewContentView.frame = CGRectMake([self leftUtilityButtonsWidth], 0, CGRectGetWidth(self.bounds), _height);
}

#pragma mark - Setup helpers

- (CGFloat)leftUtilityButtonsWidth {
    return [_scrollViewButtonViewLeft utilityButtonsWidth];
}

- (CGFloat)rightUtilityButtonsWidth {
    return [_scrollViewButtonViewRight utilityButtonsWidth];
}

- (CGFloat)utilityButtonsPadding {
    return ([_scrollViewButtonViewLeft utilityButtonsWidth] + [_scrollViewButtonViewRight utilityButtonsWidth]);
}

- (CGPoint)scrollViewContentOffset {
    return CGPointMake([_scrollViewButtonViewLeft utilityButtonsWidth], 0);
}

#pragma mark UIScrollView helpers

- (void)scrollToRight:(inout CGPoint *)targetContentOffset{
    targetContentOffset->x = [self utilityButtonsPadding];
    _cellState = kCellStateRight;
}

- (void)scrollToCenter:(inout CGPoint *)targetContentOffset {
    targetContentOffset->x = [self leftUtilityButtonsWidth];
    _cellState = kCellStateCenter;
}

- (void)scrollToLeft:(inout CGPoint *)targetContentOffset{
    targetContentOffset->x = 0;
    _cellState = kCellStateLeft;
}

- (void)wayToOriginalScrollView{
    [cellScrollView setContentOffset:CGPointMake(0, cellScrollView.frame.origin.y) animated:YES];
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    switch (_cellState) {
        case kCellStateCenter:
            if (velocity.x >= 0.5f) {
                [self scrollToRight:targetContentOffset];
            } else if (velocity.x <= -0.5f) {
                [self scrollToLeft:targetContentOffset];
            } else {
                CGFloat rightThreshold = [self utilityButtonsPadding] - ([self rightUtilityButtonsWidth] / 2);
                CGFloat leftThreshold = [self leftUtilityButtonsWidth] / 2;
                if (targetContentOffset->x > rightThreshold)
                    [self scrollToRight:targetContentOffset];
                else if (targetContentOffset->x < leftThreshold)
                    [self scrollToLeft:targetContentOffset];
                else
                    [self scrollToCenter:targetContentOffset];
            }
            break;
        case kCellStateLeft:
            if (velocity.x >= 0.5f) {
                [self scrollToCenter:targetContentOffset];
            } else if (velocity.x <= -0.5f) {
                // No-op
            } else {
                if (targetContentOffset->x > [self leftUtilityButtonsWidth] / 2)
                    [self scrollToCenter:targetContentOffset];
                else
                    [self scrollToLeft:targetContentOffset];
            }
            break;
        case kCellStateRight:
            if (velocity.x >= 0.5f) {
                // No-op
            } else if (velocity.x <= -0.5f) {
                [self scrollToCenter:targetContentOffset];
            } else {
                if (targetContentOffset->x < ([self utilityButtonsPadding] - [self rightUtilityButtonsWidth] / 2))
                    [self scrollToCenter:targetContentOffset];
                else
                    [self scrollToRight:targetContentOffset];
            }
            break;
        default:
            break;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.x > [self leftUtilityButtonsWidth]) {
        // Expose the right button view
        self.scrollViewButtonViewRight.frame = CGRectMake(scrollView.contentOffset.x + (CGRectGetWidth(self.bounds) - [self rightUtilityButtonsWidth]), 0.0f, [self rightUtilityButtonsWidth], _height);
    } else {
        // Expose the left button view
        self.scrollViewButtonViewLeft.frame = CGRectMake(scrollView.contentOffset.x, 0.0f, [self leftUtilityButtonsWidth], _height);
    }
}

@end

#pragma mark NSMutableArray class extension helper

@implementation NSMutableArray (SWUtilityButtons)

- (void)addUtilityButtonWithColor:(UIColor *)color title:(NSString *)title {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = color;
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addObject:button];
}

- (void)addUtilityButtonWithColor:(UIColor *)color icon:(UIImage *)icon {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = color;
    button.tag = 10;
    [button setImage:icon forState:UIControlStateNormal];
    [self addObject:button];
}

- (void)addUtilityButtonWithColor:(UIColor *)color icon:(UIImage *)icon andTag:(int)tag{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = color;
    button.tag = tag;
    [button setImage:icon forState:UIControlStateNormal];
    [self addObject:button];
}

@end

