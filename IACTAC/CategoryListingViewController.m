//
//  CategoryListingViewController.m
//  fyre
//
//  Created by Zayar on 3/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "CategoryListingViewController.h"
#import "Products.h"
#import "Utility.h"
#import "ProductsCollectionHeaderView.h"
#import "ProductsViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "ProductDetailViewController2.h"
@interface CategoryListingViewController ()
{
    NSArray * arrProducts;
    IBOutlet UICollectionView * productCollectionView;
    ProductDetailViewController2 * productDetailViewController2;
}
@end

@implementation CategoryListingViewController
@synthesize product_cate;
- (void)viewDidLoad
{
    [super viewDidLoad];
	//Do any additional setup after loading the view.
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    if ([Utility isGreaterOREqualOSVersion:@"7.0"] ) {
        [flowLayout setItemSize:CGSizeMake(276/2, 268/2)];
    }
    else{
        [flowLayout setItemSize:CGSizeMake(276/2, 268/2)];
    }
    
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    //[flowLayout setHeaderReferenceSize:CGSizeMake(320, 60)];
    [productCollectionView setCollectionViewLayout:flowLayout];
    [productCollectionView setAllowsSelection:YES];
    [flowLayout setSectionInset:UIEdgeInsetsMake(20, 14, 20, 14)];
    //productCollectionView set
    
    [productCollectionView setBackgroundColor:[UIColor clearColor]];
}

- (void)viewWillAppear:(BOOL)animated{
    NSPredicate *prodPredicate = [NSPredicate predicateWithFormat:@"cateID == %@",product_cate.server_id];
    NSArray * arr = [Products findAllWithPredicate:prodPredicate];
    [self loadTheData:arr];
    self.title = product_cate.name;
    
}

- (void)loadTheData:(NSArray *)arr{
    arrProducts = arr;
    [productCollectionView reloadData];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [arrProducts count];
}

/*- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        ProductsCollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        
        NSString * cateID = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSLog(@"cate id %@",cateID);
        //NSString *title = [[NSString alloc]initWithFormat:@"Product #%i", indexPath.section + 1];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"server_id == %@",cateID];
        
        Product_Category * cate = [Product_Category MR_findFirstWithPredicate:predicate];
        headerView.title.frame = CGRectMake(15, 3, 290, 30);
        headerView.title.text =[NSString stringWithFormat:@"%@",cate.name];
        headerView.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
        headerView.title.textColor = [UIColor colorWithHexString:@"6C6C6C"];
        headerView.title.font = [UIFont fontWithName:@"AvenirNext-Medium" size:14.0f];
        
        [Utility makeBorder:headerView andWidth:1 andColor:[UIColor colorWithHexString:@"DADADA"]];
        
        reusableview = headerView;
    }
    
    return reusableview;
}*/

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    ProductsViewCell *cell = (ProductsViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    Products * obj =  [arrProducts objectAtIndex:indexPath.row];
    //recipeImageView.image = [UIImage imageNamed:[recipeImages[indexPath.section] objectAtIndex:indexPath.row]];
    [recipeImageView setImageWithURL:[NSURL URLWithString:obj.image_url]];
    
    UILabel *lblName = (UILabel *)[cell viewWithTag:99];
    CGRect newRect = lblName.frame;
    if ([Utility isGreaterOREqualOSVersion:@"7.0"] ) {
        lblName.frame = newRect;
    }else{
        newRect.origin.y -= 5;
        lblName.frame = newRect;
    }
    lblName.text = obj.name;
    lblName.textColor = [UIColor colorWithHexString:@"6C6C6C"];
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo-frame-2.png"]];
    [cell logOfUsing];
    cell.backgroundColor = [UIColor whiteColor];
    [Utility makeCornerRadius:cell andRadius: 3];
    [Utility makeBorder:cell andWidth:.3 andColor:[UIColor lightGrayColor]];
    [Utility makeBorder:recipeImageView andWidth:.5 andColor:[UIColor lightGrayColor]];
    return cell;
}

#pragma mark - delegate methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    printf("Selected View index=%d",indexPath.row);
    Products * obj = [arrProducts objectAtIndex:indexPath.row];
    
    if (productDetailViewController2 == nil) {
        productDetailViewController2 = [[ProductDetailViewController2 alloc] init];
    }
    productDetailViewController2.hidesBottomBarWhenPushed = YES;
    productDetailViewController2.objProduct = obj;
    
    [self.navigationController pushViewController:productDetailViewController2 animated:YES];
    
}



@end
