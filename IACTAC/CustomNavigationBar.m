//
//  CustomNavigationBar.m
//  GroovyMap
//
//  Created by Tonytoons on 1/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CustomNavigationBar.h"
//#import "NewsHubAppDelegate.h"

@implementation CustomNavigationBar

- (id)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
	//NewsHubAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	UIImage *image = [UIImage imageNamed: @"nav_bar.png"] ;
	[image drawInRect:rect];
    
    self.backgroundColor = [UIColor clearColor];
}

#pragma mark -
#pragma mark UINavigationDelegate Methods

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
	NSString *title = viewController.navigationItem.title;
   
	UILabel *myTitleView = [[UILabel alloc] init];
	[myTitleView setFont:[UIFont boldSystemFontOfSize:18]];
	[myTitleView setTextColor: [UIColor blackColor]];
	[myTitleView setShadowColor: [UIColor whiteColor]];
	myTitleView.text = title;
    myTitleView.backgroundColor = [UIColor clearColor];
	[myTitleView sizeToFit];
	viewController.navigationItem.titleView = myTitleView;
	
   
	viewController.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.16f green:0.36f blue:0.46 alpha:0.8];
}

- (void) updateTitle:(UIViewController *)viewController{
	NSString *title = viewController.navigationItem.title;
	
	UILabel *myTitleView = [[UILabel alloc] init];
	[myTitleView setFont:[UIFont boldSystemFontOfSize:18]];
	[myTitleView setTextColor: [UIColor blackColor] ];
	[myTitleView setShadowColor: [UIColor whiteColor]];
	myTitleView.text = title;
    myTitleView.backgroundColor = [UIColor clearColor];
	[myTitleView sizeToFit];
	viewController.navigationItem.titleView = myTitleView;
	
	viewController.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.16f green:0.36f blue:0.46 alpha:0.8];
}

#pragma mark TextField

- (void)textFieldDidBeginEditing:(UITextField *)textField{
   
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
}


@end
