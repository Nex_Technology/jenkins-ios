//
//  ViewController.m
//  IACTAC
//
//  Created by Zayar on 10/29/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "ProductsViewController.h"
#import "CustomScrollSliderView.h"
#import "Product_Category.h"
#import "Products.h"
#import "ProductsCollectionHeaderView.h"
#import "ProductsViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "Utility.h"
#import "ProductDetailViewController.h"
#import "ProductDetailViewController2.h"
#import "CategoryListingViewController.h"
#import "AFHTTPRequestOperation.h"
#import "AppDelegate.h"
#import "SliderItem.h"
@interface ProductsViewController ()
{
    CustomScrollSliderView * customSlider;
    NSArray * arrProducts;
    IBOutlet UICollectionView * productCollectionView;
    ProductDetailViewController * productDetailViewController;
    ProductDetailViewController2 * productDetailViewController2;
    CategoryListingViewController * cateListingViewController;
    
    IBOutlet UIScrollView * scrollView;
    float scrollContentIncremental;
}
@property (strong, nonatomic) NSMutableDictionary *sections;
@end

@implementation ProductsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if ([Utility isGreaterOREqualOSVersion:@"7.0"] ) {
        if (screenBounds.size.height == 568) {
//            customSlider = [[CustomScrollSliderView alloc] initWithFrame:CGRectMake(0, -44, 320, 200)];
//            [productCollectionView setFrame:CGRectMake(0, 157, 320, 370+40)];
            customSlider = [[CustomScrollSliderView alloc] initWithFrame:CGRectMake(0, 64, 320, 200)];
            [productCollectionView setFrame:CGRectMake(0, 157+107, 320, 370+107)];
            scrollContentIncremental = 44;
        }
        else{
            customSlider = [[CustomScrollSliderView alloc] initWithFrame:CGRectMake(0, 64+88, 320, 200)];
            [productCollectionView setFrame:CGRectMake(0, 157+107+88, 320, 370+107)];
            scrollContentIncremental = 44+88;
        }
    }else{
        
        if (screenBounds.size.height == 568) {
            //            customSlider = [[CustomScrollSliderView alloc] initWithFrame:CGRectMake(0, -44, 320, 200)];
            //            [productCollectionView setFrame:CGRectMake(0, 157, 320, 370+40)];
            customSlider = [[CustomScrollSliderView alloc] initWithFrame:CGRectMake(0, 105, 320, 175)];
            [productCollectionView setFrame:CGRectMake(0, 175+105, 320, 370)];
            scrollContentIncremental = 105;
        }
        else{
            customSlider = [[CustomScrollSliderView alloc] initWithFrame:CGRectMake(0, 105+88+5, 320, 175)];
            [productCollectionView setFrame:CGRectMake(0, 175+105+88+5, 320, 370)];
            scrollContentIncremental = 105+88+5;
        }
        
    }
    
    customSlider.owner = self;
    [scrollView addSubview:customSlider];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    if ([Utility isGreaterOREqualOSVersion:@"7.0"] ) {
        [flowLayout setItemSize:CGSizeMake(276/2, 268/2)];
    }
    else{
        [flowLayout setItemSize:CGSizeMake(276/2, 268/2)];
    }
    
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setHeaderReferenceSize:CGSizeMake(320, 60)];
    [productCollectionView setCollectionViewLayout:flowLayout];
    [productCollectionView setAllowsSelection:YES];
    [flowLayout setSectionInset:UIEdgeInsetsMake(20, 14, 20, 14)];
    //productCollectionView set
    
    [productCollectionView setBackgroundColor:[UIColor clearColor]];
    //[self.view setBackgroundColor:[UIColor colorWithHexString:@"F0F0F0"]];
    
    flowLayout.headerReferenceSize = CGSizeMake(0, 35);
    
    productCollectionView.scrollEnabled = NO;
    
   // NSLog(@"product collection content size width %f and height %f",productCollectionView.contentSize.width,productCollectionView.contentSize);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTheView) name:@"refreshProductView" object:nil];
    
    UIBarButtonItem * btnOrder = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_refresh"] style:UIBarButtonItemStylePlain target:self action:@selector(refreshTheData:)];
    self.navigationItem.rightBarButtonItem = btnOrder;
}

- (void)refreshTheData:(UIBarButtonItem *)sender{
    AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    [SVProgressHUD show];
    [delegate syncProducts];
}

- (void) viewWillAppear:(BOOL)animated{
    [self refreshTheView];
}

- (void)refreshTheView{
    NSArray * arr = [Product_Category findAll];
    NSMutableArray * arrStr = [[NSMutableArray alloc] init];
    NSLog(@"arr string count %d",[arr count]);
    for (Product_Category * pCate in arr) {
        SliderItem * obj = [[SliderItem alloc] init];
        obj.strUrl = pCate.image_url;
        obj.strUniqId  = pCate.server_id;
        [arrStr addObject:obj];
    }
    if ([arrStr count]>0) {
        [customSlider loadImageViewWith:(NSArray *)arrStr];
    }
    arrProducts = [Products findAll];
    if ([arrProducts count]>0) {
         [self loadTheProductWithSection:arrProducts];
    }
    
    NSLog(@"product collection content size width %f and height %f",productCollectionView.collectionViewLayout.collectionViewContentSize.width,productCollectionView.collectionViewLayout.collectionViewContentSize.height);
    
    [scrollView setContentSize:CGSizeMake(320, customSlider.frame.size.height + productCollectionView.collectionViewLayout.collectionViewContentSize.height+ scrollContentIncremental)];
    
    CGRect newFrame = productCollectionView.frame;
    newFrame.size.height = productCollectionView.collectionViewLayout.collectionViewContentSize.height;
    productCollectionView.frame = newFrame;
}

- (void) loadTheProductWithSection:(NSArray *)arrProd{
    if (self.sections != nil) {
        if ([[self.sections allKeys] count]>0) {
            [self.sections removeAllObjects];
        }
    }
    self.sections = [NSMutableDictionary dictionary];
    BOOL found;
    for (Products * obj in arrProd)
    {
        NSString *c = obj.cateID;
        NSLog(@"cateid %@",c);
        found = NO;
        
        for (NSString *str in [self.sections allKeys])
        {
            
            NSLog(@"key string: %@",str);
            if ([str isEqualToString:c])
            {
                NSLog(@"arr string: %@",c);
                found = YES;
            }
        }
        
        if (!found)
        {
            [self.sections setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    // Loop again and sort the books into their respective keys
    for (Products * obj in arrProd)
    {
        [[self.sections objectForKey:obj.cateID] addObject:obj];
        //[[self.sections objectForKey:obj.cateID] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]]];
    }
    
    // Sort each section array
    /*for (NSString *key in [self.sections allKeys])
    {
        [[self.sections objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]]];
    }*/
    
    [productCollectionView reloadData];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    
    return [self.sections count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSLog(@"section %d and product count %d",section,[[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count]);
    return [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        ProductsCollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        
        NSString * cateID = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSLog(@"cate id %@",cateID);
        //NSString *title = [[NSString alloc]initWithFormat:@"Product #%i", indexPath.section + 1];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"server_id == %@",cateID];
        
        Product_Category * cate = [Product_Category MR_findFirstWithPredicate:predicate];
        headerView.title.frame = CGRectMake(15, 3, 290, 30);
        headerView.title.text =[NSString stringWithFormat:@"%@",cate.name];
        headerView.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
        headerView.title.textColor = [UIColor colorWithHexString:@"6C6C6C"];
        headerView.title.font = [UIFont fontWithName:@"AvenirNext-Medium" size:14.0f];
        
        [Utility makeBorder:headerView andWidth:1 andColor:[UIColor colorWithHexString:@"DADADA"]];
        
        reusableview = headerView;
    }
    
    return reusableview;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    ProductsViewCell *cell = (ProductsViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    Products * obj =  [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    //recipeImageView.image = [UIImage imageNamed:[recipeImages[indexPath.section] objectAtIndex:indexPath.row]];
    [recipeImageView setImageWithURL:[NSURL URLWithString:obj.image_url]];
    
    UILabel *lblName = (UILabel *)[cell viewWithTag:99];
    CGRect newRect = lblName.frame;
    if ([Utility isGreaterOREqualOSVersion:@"7.0"] ) {
        lblName.frame = newRect;
    }else{
        newRect.origin.y -= 5;
        lblName.frame = newRect;
    }
    lblName.text = obj.name;
    lblName.textColor = [UIColor colorWithHexString:@"6C6C6C"];
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo-frame-2.png"]];
    [cell logOfUsing];
    cell.backgroundColor = [UIColor whiteColor];
    [Utility makeCornerRadius:cell andRadius: 3];
    [Utility makeBorder:cell andWidth:.3 andColor:[UIColor lightGrayColor]];
    [Utility makeBorder:recipeImageView andWidth:.5 andColor:[UIColor lightGrayColor]];
    return cell;
}

#pragma mark - delegate methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    printf("Selected View index=%d",indexPath.row);
    /*Employee * emp = [arrEmpList objectAtIndex:indexPath.row];
     [self syncLoginWith:emp.username];*/
    Products * obj = [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
            if (productDetailViewController2 == nil) {
            productDetailViewController2 = [[ProductDetailViewController2 alloc] init];
        }
       productDetailViewController2.hidesBottomBarWhenPushed = YES;
        //[productDetailViewController2 loadTheView:obj];
    productDetailViewController2.objProduct = obj;
        [self.navigationController pushViewController:productDetailViewController2 animated:YES];
//    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  mark - sliderDelegate methods
- (void) onImageClick:(CustomScrollSliderView *)sliderView andObjectImage:(SliderItem *) sItem{
    //NSLog(@"here is slider click!! ");
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"server_id == %@",sItem.strUniqId];
    Product_Category * cate = [Product_Category findFirstWithPredicate:predicate inContext:localContext];
    
    if (cateListingViewController == nil) {
        cateListingViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CateListing"];
        
    }
    cateListingViewController.product_cate = cate;
     [self.navigationController pushViewController:cateListingViewController animated:YES];
}

@end
