//
//  NewsViewController.m
//  IACTAC
//
//  Created by Zayar on 10/29/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "NewsViewController.h"
#import "News.h"
#import "NewsCell.h"
#import "NewsDetailViewController.h"
#import "XCDYouTubeVideoPlayerViewController.h"
#import "Utility.h"
#import "StringTable.h"
#import "AppDelegate.h"
@interface NewsViewController ()
{
    IBOutlet UITableView * tbl;
    NSArray * arr;
    NewsDetailViewController * newsDetailViewController;
}
@end

@implementation NewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ([tbl respondsToSelector:@selector(separatorInset)]) {
        [tbl setSeparatorInset:UIEdgeInsetsZero];
    }
    
    UIBarButtonItem * btnOrder = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_refresh"] style:UIBarButtonItemStylePlain target:self action:@selector(refreshTheData:)];
    self.navigationItem.rightBarButtonItem = btnOrder;
}

- (void)refreshTheData:(UIBarButtonItem *)sender{
    AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    [SVProgressHUD show];
    [delegate syncNews];
}

- (void) viewWillAppear:(BOOL)animated{
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
    arr = [News MR_findAllInContext:localContext];
    NSLog(@"arr count %d",[arr count]);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NewsCell";
	NewsCell *cell = (NewsCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[NewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    
    //Service * obj = [arrCart objectAtIndex:indexPath.row];
    News * obj = [arr   objectAtIndex:indexPath.row];
    [cell loadTheView:obj];
    return cell;
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 88;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    News * obj = [arr objectAtIndex:indexPath.row];
    NSLog(@"obj news cate %@",obj.category);
    if ([obj.category isEqualToString:NEWS_CATE_VIDEO]) {
        NSString * strUTubeId = [Utility extractYoutubeID:obj.url];
        XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:strUTubeId];
        [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
    }
    else {
        if (newsDetailViewController == nil) {
            newsDetailViewController = [[NewsDetailViewController alloc] init];
        }
        [newsDetailViewController loadTheViewWithNews:obj];
        [self.navigationController pushViewController:newsDetailViewController animated:YES];
        [self.navigationItem.backBarButtonItem setTintColor:[UIColor redColor]];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [arr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
