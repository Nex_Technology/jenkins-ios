//
//  OrderViewController.h
//  IACTAC
//
//  Created by Zayar on 12/3/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NXViewController.h"
#import "Products.h"
@interface OrderViewController : NXViewController
@property (nonatomic, strong) Products * objProduct;
@end
