//
//  Product_Category.h
//  IACTAC
//
//  Created by Zayar on 10/31/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Product_Category : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * image_url;
@property (nonatomic, retain) NSNumber * is_active;
@property (nonatomic, retain) NSString * local_id;
@property (nonatomic, retain) NSString * server_id;
@property (nonatomic, retain) NSString * thumb;
@property (nonatomic, retain) NSNumber * timetick;
@property (nonatomic, retain) NSNumber * updated_time;
@property (nonatomic, retain) NSString * name;

@end
