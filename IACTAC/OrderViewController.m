//
//  OrderViewController.m
//  IACTAC
//
//  Created by Zayar on 12/3/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "OrderViewController.h"
#import "ContactCell.h"
#import "Utility.h"
#import "StringTable.h"
#import "IacAPIClient.h"
#import "Config.h"
@interface OrderViewController ()
{
    IBOutlet UITableView * tbl;
    UITextView * txtMessage;
    IBOutlet UIToolbar * toolbar;
    
    UILabel * lblbtnCap2;
    UILabel * lblResult;
    int selectedQty;
    int selectedAmount;
    NSMutableArray * arrQty;
    
    NSString * strFName;
    NSString * strLName;
    NSString * strEmail;
    NSString * strPhone;
    NSString * strMessage;
    NSString * strAddress;
    
    int statusOfTextField;
}
@property (nonatomic,strong) UITableViewCell *btnCellQty;
@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) UIActionSheet *menu;
@end

@implementation OrderViewController
#define animation_delay 0.5
#define max_qty 99
@synthesize objProduct;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if ([tbl respondsToSelector:@selector(separatorInset)]) {
        [tbl setSeparatorInset:UIEdgeInsetsZero];
    }
    
    UIBarButtonItem * sendItem = [[UIBarButtonItem alloc] initWithTitle:@"Send" style:UIBarButtonItemStylePlain target:self action:@selector(onSend:)];
    //sendItem.tintColor = [UIColor redColor];
    self.navigationItem.rightBarButtonItem = sendItem;
    
    /*if ([tbl respondsToSelector:@selector(separatorInset)]) {
     [tbl setSeparatorInset:UIEdgeInsetsZero];
     }*/
    
    txtMessage = [[UITextView alloc]initWithFrame:CGRectMake(20, 0, 280, 80)];
    txtMessage.delegate = self;
    txtMessage.font = [UIFont fontWithName:@"AvenirNext-Medium" size:16];
    txtMessage.textColor = [UIColor colorWithHexString:@"6C6C6C"];
    
    //UIView * v = [[UIView alloc] init];
    //[tbl setSectionFooterHeight:0];
    
    //---registers the notifications for keyboard---
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    tbl.backgroundColor = [UIColor colorWithHexString: @"f0f0f0"];
    
    //UIView * view = [[UIView alloc] init];
    tbl.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 10.0f)];
    
    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,170,0,0)];
    
    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    self.menu = [[UIActionSheet alloc] initWithTitle:@"Select Pet"
                                            delegate:self
                                   cancelButtonTitle:@"Done"
                              destructiveButtonTitle:@"Cancel"
                                   otherButtonTitles:nil];
    
    
    selectedQty = 0;
    selectedAmount = 1;
    [self hardcodeQty];
    NSLog(@"arr qty count %d",[arrQty count]);
    statusOfTextField = 0;
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"contact view appear!!");
    [tbl reloadData];
    
}

- (void)hardcodeQty{
    if (!arrQty) {
        arrQty = [[NSMutableArray alloc] initWithCapacity:max_qty];
    }
    for ( int i = 1; i<=max_qty; i++) {
        NSString * str = [NSString stringWithFormat:@"%d",i];
        [arrQty addObject:str];
    }
}

- (void)onSend:(UIBarButtonItem *)sender{
    NSLog(@"Send!!!");
    [self validateTheFieldAndSend];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ContactCell";
    static NSString *CellIdentifierText = @"ContactCellTextView";
	ContactCell *cell = (ContactCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[ContactCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    [cell loadTextWith:@"First Name" andPlaceHolder:@"first name"];
    
    //Service * obj = [arrCart objectAtIndex:indexPath.row];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [cell loadTextWith:@"First Name" andPlaceHolder:@"first name"];
            cell.txtValue.tag = 1;
            cell.txtValue.returnKeyType = UIReturnKeyNext;
        }
        else if(indexPath.row == 1){
            [cell loadTextWith:@"Last Name" andPlaceHolder:@"last name"];
            cell.txtValue.tag = 7;
            cell.txtValue.returnKeyType = UIReturnKeyNext;
        }
    }
    else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [cell loadTextWith:@"Email" andPlaceHolder:@"email"];
            cell.txtValue.tag = 3;
            cell.txtValue.returnKeyType = UIReturnKeyNext;
        }
        else if(indexPath.row == 1){
            [cell loadTextWith:@"Phone" andPlaceHolder:@"phone"];
            cell.txtValue.tag = 4;
            cell.txtValue.returnKeyType = UIReturnKeyNext;
        }
    }
    else if (indexPath.section == 2) {
        /*UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell2 == nil) {
            cell2 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierText];
        }
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        cell2.tag = indexPath.row;
        txtMessage.tag = 5;
        txtMessage.delegate = self;
        [cell2 addSubview:txtMessage];
        txtMessage.returnKeyType = UIReturnKeyDone;
        return cell2;*/
        if (indexPath.row == 0) {
            /*[cell loadTextWith:@"Qty" andPlaceHolder:@"quantity"];
            cell.txtValue.tag = 5;
            cell.txtValue.returnKeyType = UIReturnKeyNext;
            cell.txtValue.keyboardType = UIKeyboardTypeNumberPad;*/
            self.btnCellQty = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
            if (!lblbtnCap2) {
                lblbtnCap2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 120, 30)];
            }
            lblbtnCap2.text = @"Qty";
            //lblbtnCap2.textColor = [UIColor blackColor];
            lblbtnCap2.textColor = [UIColor colorWithHexString:@"6C6C6C"];
            lblbtnCap2.font = [UIFont fontWithName:@"AvenirNext-Medium" size:16];
            lblbtnCap2.textAlignment = NSTextAlignmentLeft;
            lblbtnCap2.font = [UIFont boldSystemFontOfSize:13];
            lblbtnCap2.backgroundColor = [UIColor clearColor];
            lblbtnCap2.tag = 1;
            if (!lblResult) {
               lblResult = [[UILabel alloc]initWithFrame:CGRectMake(130, 7, 150, 30)];
            }
            lblResult.text = [NSString stringWithFormat:@"%d",selectedAmount ];
            lblResult.textAlignment = NSTextAlignmentRight;
            lblResult.font = [UIFont fontWithName:@"AvenirNext-Regular" size:16];
            lblResult.textAlignment = UITextAlignmentRight;
            lblResult.font = [UIFont boldSystemFontOfSize:13];
            lblResult.backgroundColor = [UIColor clearColor];
            lblResult.tag = 2;
            self.btnCellQty.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            self.btnCellQty.selectionStyle = UITableViewCellSelectionStyleNone;
            [self.btnCellQty addSubview:lblbtnCap2];
            [self.btnCellQty addSubview:lblResult];
            
            return self.btnCellQty;
        }
        else if(indexPath.row == 1){
            [cell loadTextWith:@"Presonal" andPlaceHolder:@"address"];
            cell.txtValue.tag = 5;
            cell.txtValue.returnKeyType = UIReturnKeyDone;
        }
        
    }
    else if (indexPath.section == 3) {
        UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell2 == nil) {
            cell2 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierText];
        }
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        cell2.tag = indexPath.row;
        txtMessage.tag = 5;
        txtMessage.delegate = self;
        [cell2 addSubview:txtMessage];
        txtMessage.returnKeyType = UIReturnKeyDone;
        return cell2;
    }
    
    cell.txtValue.delegate = self;
    return cell;
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 3) {
        return 80;
    }
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*if (newsDetailViewController == nil) {
     newsDetailViewController = [[NewsDetailViewController alloc] init];
     }
     News * obj = [arr objectAtIndex:indexPath.row];
     [newsDetailViewController loadTheViewWithNews:obj];
     [self.navigationController pushViewController:newsDetailViewController animated:YES];
     [self.navigationItem.backBarButtonItem setTintColor:[UIColor redColor]];*/
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            [self showQtySelector];
        }
    }
}

- (void)showQtySelector{
    [self.menu setTitle:@"Pick qty"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 0;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:selectedQty inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,600)];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    if (section == 0) {
        return 2;
    }
    else if(section == 1){
        return 2;
    }
    else if(section == 2){
        return 1;
    }
    else if(section == 3){
        return 1;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 4;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CGRect newFrame = v.frame;
    newFrame.origin.x = 16;
    newFrame.origin.y = 25;
    newFrame.size.height = 20;
    UILabel * lbl = [[UILabel alloc] initWithFrame:newFrame];
    lbl.font = [UIFont fontWithName:@"AvenirNext-Regular" size:14];
    lbl.textColor = [UIColor colorWithHexString:@"6C6C6C"];
    lbl.backgroundColor = [UIColor clearColor];
    [v setBackgroundColor:[UIColor colorWithHexString:@"f0f0f0"]];
    
    if (section == 0) {
        lbl.text = @" PERSONAL";
    }
    else if(section == 1){
        lbl.text = @" CONTACT";
    }
    else if(section == 2){
        lbl.text = @" ORDER INFO";
    }
    else if(section == 3){
        lbl.text = @" ADDRESS";
    }
    
    [v addSubview:lbl];
    [Utility makeBorder:v andWidth:0.5 andColor:[UIColor lightGrayColor]];
    return v;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard--
    
    /*if (!self.contactView.hidden) {
     if ( (206 - 47) != self.qdController.quickDialogTableView.frame.origin.y) {
     NSLog(@"key board will show!! %0.2f",self.qdController.quickDialogTableView.frame.origin.y);
     [UIView animateWithDuration:animation_delay animations:^{
     [self.qdController.quickDialogTableView setFrame:CGRectMake(0, 206 - 47, self.qdController.quickDialogTableView.frame.size.width, self.qdController.quickDialogTableView.frame.size.height+ 80)];
     }];
     }
     }
     if (!self.customerReadOnlyView.hidden) {
     if ( (206 - 47) != self.qdROController.quickDialogTableView.frame.origin.y) {
     NSLog(@"key board will show!! %0.2f",self.qdROController.quickDialogTableView.frame.origin.y);
     [UIView animateWithDuration:animation_delay animations:^{
     [self.qdROController.quickDialogTableView setFrame:CGRectMake(0, 206 - 47, self.qdROController.quickDialogTableView.frame.size.width, self.qdROController.quickDialogTableView.frame.size.height+ 80)];
     }];
     }
     }*/
    /*[UIView animateWithDuration:animation_delay animations:^{
     [tbl setFrame:CGRectMake(0, -50, tbl.frame.size.width, tbl.frame.size.height+ 80)];
     }];*/
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect frame = toolbar.frame;
    frame.origin.y = self.view.frame.size.height - 260.0;
    toolbar.frame = frame;
    
    [UIView commitAnimations];
    
}

-(IBAction)resignKeyboard:(id)sender {
    [self.view endEditing:YES];
}

-(void) keyboardWillHide:(NSNotification *) notification {
    /*NSLog(@"key board will hide!!");
     searchTxtField.text = @"";
     tblPersonList.tag = 0;
     [tblPersonList reloadData];
     if (!self.contactView.hidden) {
     [UIView animateWithDuration:animation_delay animations:^{
     [self.qdController.quickDialogTableView setFrame:CGRectMake(0, 206, self.qdController.quickDialogTableView.frame.size.width, self.qdController.quickDialogTableView.frame.size.height - 80)];
     }];
     }
     if (!self.customerReadOnlyView.hidden) {
     [UIView animateWithDuration:animation_delay animations:^{
     [self.qdROController.quickDialogTableView setFrame:CGRectMake(0, 206, self.qdROController.quickDialogTableView.frame.size.width, self.qdROController.quickDialogTableView.frame.size.height - 80)];
     }];
     }*/
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect frame = toolbar.frame;
    frame.origin.y = self.view.frame.size.height;
    toolbar.frame = frame;
    
    [UIView commitAnimations];
    
    //[self reMoveToTopTable:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"text tag!! %d",textField.tag);
    if(textField.tag == 1) {
        UIView * nextView = [self.view viewWithTag:7];
        [nextView becomeFirstResponder];
        return NO;
    }
    else if(textField.tag == 7) {
        UIView * nextView = [self.view viewWithTag:3];
        [nextView becomeFirstResponder];
        return NO;
    }
    else if(textField.tag == 3) {
        UIView * nextView = [self.view viewWithTag:4];
        [nextView becomeFirstResponder];
        return NO;
    }
    else if(textField.tag == 4) {
        NSLog(@"here is tag 4!!");
        UIView * nextView = [self.view viewWithTag:5];
        [nextView becomeFirstResponder];
        return NO;
    }
    /*else if(textField.tag == 5) {
        NSLog(@"here is tag 5!!");
        UIView * nextView = [self.view viewWithTag:6];
        [nextView becomeFirstResponder];
        return NO;
    }*/
    
    [textField resignFirstResponder];
	return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"should start!!");
    
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    [self moveToTopTable:textView.tag];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [self reMoveToTopTable:textView.tag];
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag == 4 || textField.tag == 5||textField.tag == 6) {
        NSLog(@"begin edited!! tag %d",textField.tag);
        [self moveToTopTable:textField.tag];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"end editing!!!");
    if (textField.tag == 6) {
        [self reMoveToTopTable:textField.tag];
    }
}

- (void)moveToTopTable:(int)tag{
    if (tag == 4) {
        [UIView animateWithDuration:animation_delay animations:^{
            [tbl setFrame:CGRectMake(0, -180, tbl.frame.size.width, tbl.frame.size.height)];
        }];
        statusOfTextField = tag;
    }
    if (tag == 5) {
        [UIView animateWithDuration:animation_delay animations:^{
            [tbl setFrame:CGRectMake(0, -240, tbl.frame.size.width, tbl.frame.size.height)];
        }];
        statusOfTextField = tag;
    }
}

- (void)reMoveToTopTable:(int)tag{
    //if (tag == 5) {
    NSLog(@"here!!! and tag %d",tag);
        [UIView animateWithDuration:animation_delay animations:^{
            [tbl setFrame:CGRectMake(0,0, tbl.frame.size.width, tbl.frame.size.height)];
        }];
    //}
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        NSLog(@"return!!");
        [textView resignFirstResponder];
        [self reMoveToTopTable:textView.tag];
        return NO;
    }
    
    return YES;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView.tag == 0) {
        NSString * obj = [arrQty objectAtIndex:row];
        return obj;
    }
    
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        return [arrQty count];
    }
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 0) {
        selectedQty = row;
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
        [self.uiPickerView removeFromSuperview];
    }
    
    else if (buttonIndex == 1) {
        //NSLog(@"Other Button Done Clicked and selected index %d",self.selectedType);
        [self.uiPickerView removeFromSuperview];
        if (self.uiPickerView.tag == 0){
            NSString * str = [arrQty objectAtIndex:selectedQty];
            lblResult.text = str;
            selectedAmount = [str intValue];
        }
        
    }
}

- (void)validateTheFieldAndSend{
    NSIndexPath * indexFName = [NSIndexPath indexPathForRow:0 inSection:0];
    ContactCell * fNameCell = (ContactCell *)[tbl cellForRowAtIndexPath:indexFName];
    if ([Utility stringIsEmpty:fNameCell.txtValue.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:ERRMSG1];
        strFName = @"";
        return;
    }
    else strFName =fNameCell.txtValue.text;
    
    NSIndexPath * indexLName = [NSIndexPath indexPathForRow:1 inSection:0];
    ContactCell * lNameCell = (ContactCell *)[tbl cellForRowAtIndexPath:indexLName];
    if ([Utility stringIsEmpty:lNameCell.txtValue.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:ERRMSG2];
        strLName = @"";
        return;
    }
    else strLName = lNameCell.txtValue.text;
    NSIndexPath * indexEmail = [NSIndexPath indexPathForRow:0 inSection:1];
    ContactCell * emailCell = (ContactCell *)[tbl cellForRowAtIndexPath:indexEmail];
    if ([Utility stringIsEmpty:emailCell.txtValue.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:ERRMSG12];
        strEmail = @"";
        return;
    }
    else if (![Utility validateEmailWithString:emailCell.txtValue.text]) {
        [Utility showAlert:APP_TITLE message:ERRMSG11];
        strEmail = @"";
        return;
    }
    else strEmail = emailCell.txtValue.text;
    
    NSIndexPath * indexPhone = [NSIndexPath indexPathForRow:1 inSection:1];
    ContactCell * phoneCell = (ContactCell *)[tbl cellForRowAtIndexPath:indexPhone];
    if ([Utility stringIsEmpty:phoneCell.txtValue.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:ERRMSG10];
        strPhone = @"";
        return;
    }
    else strPhone = phoneCell.txtValue.text;
    
    NSIndexPath * indexAddress = [NSIndexPath indexPathForRow:1 inSection:2];
    ContactCell * addressCell = (ContactCell *)[tbl cellForRowAtIndexPath:indexPhone];
    if ([Utility stringIsEmpty:txtMessage.text shouldCleanWhiteSpace:YES]) {
        //[Utility showAlert:APP_TITLE message:@"Please enter your phone no!"];
        strAddress = @"";
        //return;
    }
    else strAddress = txtMessage.text;
    [self onSend];
}

- (void) onSend{
    Config * conf = [Config getCurrentDefaultConfig];
    if (![Utility stringIsEmpty:conf.order_link shouldCleanWhiteSpace:YES]) {
        [SVProgressHUD show];
        NSDictionary * param = [[NSDictionary alloc] init];
        param = @{@"product_id":objProduct.server_id,@"firstName":strFName,@"lastName":strLName,@"email":strEmail,@"phone":strPhone, @"address":strAddress,@"qty":[NSString stringWithFormat:@"%d",selectedAmount]};
        
        [[IacAPIClient sharedClientWithoutBaseUrl] POST:[NSString stringWithFormat:@"%@",conf.order_link] parameters:param success:^(AFHTTPRequestOperation *operation, id json) {
            NSLog(@"successfully return!!! %@",json);
            NSDictionary * dics = (NSDictionary *)json;
            int status= [[dics objectForKey:@"status"] intValue];
            if (status == 1) {
                [SVProgressHUD showSuccessWithStatus:@"Successfully sent! We'll get back to you as soon as we can. Thanks for ordering us."];
                txtMessage.text = @"";
                selectedQty = 0;
                selectedAmount = 1;
                [tbl reloadData];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error %@",error);
            //[SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
            [SVProgressHUD dismiss];
        }];
    }
    
}

@end
