//
//  StoreCell.h
//  IACTAC
//
//  Created by Zayar on 10/30/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreCell : UITableViewCell
    {
        UILabel * lblName;
        UILabel * lblLocation;
        UIImageView * imageLocationView;
    }
@end
