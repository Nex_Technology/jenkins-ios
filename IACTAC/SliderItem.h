//
//  SliderImage.h
//  fyre
//
//  Created by Zayar on 3/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SliderItem : NSObject
@property (nonatomic, strong) NSString * strUrl;
@property (nonatomic, strong) NSString * strUniqId;
@end
