//
//  NewsCell.h
//  IACTAC
//
//  Created by Zayar on 10/29/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"
@interface NewsCell : UITableViewCell
{
    UIImageView * imgView;
    UILabel * lblName;
}
- (void) setUpViews;

- (void) loadTheView:(News *) obj;
@end
