//
//  Stores.h
//  IACTAC
//
//  Created by Zayar on 12/27/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Stores : NSManagedObject

@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * fb_link;
@property (nonatomic, retain) NSNumber * has_sync;
@property (nonatomic, retain) NSString * image_url;
@property (nonatomic, retain) NSNumber * is_active;
@property (nonatomic, retain) NSDecimalNumber * lat;
@property (nonatomic, retain) NSString * local_id;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSDecimalNumber * longtitude;
@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * server_id;
@property (nonatomic, retain) NSString * thumb;
@property (nonatomic, retain) NSNumber * timetick;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * updated_time;
@property (nonatomic, retain) NSString * fb_id;

@end
