//
//  News.h
//  IACTAC
//
//  Created by Zayar on 10/29/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface News : NSManagedObject

@property (nonatomic, retain) NSString * server_id;
@property (nonatomic, retain) NSNumber * create_time;
@property (nonatomic, retain) NSNumber * has_sync;
@property (nonatomic, retain) NSNumber * is_active;
@property (nonatomic, retain) NSString * local_id;
@property (nonatomic, retain) NSNumber * timetick;
@property (nonatomic, retain) NSNumber * updated_time;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * short_descript;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * image_url;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * category;

-(void)setTheDateFromString:(NSString *)strDate;
@end
