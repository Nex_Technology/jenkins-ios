//
//  NewsDetailViewController.m
//  IACTAC
//
//  Created by Zayar on 10/30/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "NewsDetailViewController.h"
#import "MDCParallaxView.h"
#import "News.h"
#import "UIImageView+AFNetworking.h"
#import <Twitter/Twitter.h>
#import "TSActionSheet.h"
#import "AppDelegate.h"
#import <TTTAttributedLabel/TTTAttributedLabel.h>
@interface NewsDetailViewController ()
{
    UIImageView *backgroundImageView;
    MDCParallaxView *parallaxView;
    News * objNews;
    UIActionSheet *shareActionSheet;
}
@end

@implementation NewsDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.navigationItem.backBarButtonItem.tintColor = [UIColor redColor];

    
    NSLog(@"detail view did load!!");
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(showShareActionSheet:)];
    self.navigationItem.rightBarButtonItem = shareButton;
}

- (void)loadTheViewWithNews:(News *) obj{
    objNews = obj;
    CGRect backgroundRect = CGRectMake(0, 0, 480, 404);
    backgroundImageView = [[UIImageView alloc] initWithFrame:backgroundRect];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    [backgroundImageView setImageWithURL:[NSURL URLWithString:obj.image_url] placeholderImage:[UIImage imageNamed:@"background.png"]];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [backgroundImageView addGestureRecognizer:tapGesture];
    
    CGRect textContainerRect = CGRectMake(0, 0, self.view.frame.size.width, 400.0f);
    CGRect textRect = CGRectMake(20, 10, textContainerRect.size.width - 40, textContainerRect.size.height - 40);
    
    UIView * textViewContainer = [[UIView alloc] initWithFrame: textContainerRect];
    textViewContainer.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    
    /*TTTAttributedLabel *textView = [[TTTAttributedLabel alloc] initWithFrame: textRect];

    textView.lineBreakMode = UILineBreakModeWordWrap;
    textView.textAlignment = NSTextAlignmentLeft;
    textView.font = [UIFont fontWithName:@"AvenirNext-Regular" size:16];
    textView.textColor = [UIColor colorWithHexString:@"6C6C6C"];
    textView.numberOfLines = 0;
    textView.dataDetectorTypes = NSTextCheckingAllTypes;
    [textView setText:NSLocalizedString(obj.content, nil) afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        return mutableAttributedString;
    }];
    [textViewContainer addSubview: textView];*/
    UIWebView * webView=[[UIWebView alloc] initWithFrame: textRect];
    [webView setOpaque:NO];
    [[webView scrollView]setBounces:NO];
    [[webView scrollView]setScrollEnabled:NO];
    [[webView scrollView] setShowsVerticalScrollIndicator:NO];
	[webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"AvenirNext-Regular\" size=\"2\" color='#000'> <br/> %@ </font></body></html>",obj.content] baseURL:nil];
    [webView setBackgroundColor:[UIColor whiteColor]];
    [textViewContainer addSubview: webView];
    
    CALayer * topBorder = [CALayer layer];
    topBorder.borderColor = [[UIColor colorWithHexString:@"DADADA"] CGColor];
    topBorder.borderWidth = 1.0f;
    topBorder.frame = CGRectMake(0, 0, CGRectGetWidth(textViewContainer.frame), CGRectGetHeight(textViewContainer.frame));
    
    [textViewContainer.layer addSublayer:topBorder];
    
    if (parallaxView == nil) {
       parallaxView = [[MDCParallaxView alloc] initWithBackgroundView: backgroundImageView
                                           foregroundView: textViewContainer];
    }
    parallaxView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    parallaxView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    parallaxView.backgroundHeight = 250.0f;
    parallaxView.scrollView.scrollsToTop = YES;
    parallaxView.backgroundInteractionEnabled = YES;
    parallaxView.scrollViewDelegate = self;
    [self.view addSubview:parallaxView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onShare:(UIBarButtonItem *)sender{
    [self onTwitter];
}

-(void)showActionSheet:(id)sender forEvent:(UIEvent*)event
{
    TSActionSheet *actionSheet = [[TSActionSheet alloc] initWithTitle:@"Select Source"];
    AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    actionSheet.backgroundColor = [UIColor whiteColor];
    [actionSheet addButtonWithTitle:@"Facebook" block:^{
        NSLog(@"Facebook");
        //[delegate fbShare:objNews];
        BOOL displayedNativeDialog = [FBNativeDialogs
                                      presentShareDialogModallyFrom:self
                                      initialText:@"Sharing from TAC"
                                      image:[UIImage imageNamed:@"testimage.png"]
                                      url:[NSURL URLWithString:objNews.url]
                                      handler:^(FBNativeDialogResult result, NSError *error) {
                                          
                                          NSString *alertText = @"";
                                          if ([[error userInfo][FBErrorDialogReasonKey] isEqualToString:FBErrorDialogNotSupported]) {
                                              alertText = @"iOS Share Sheet not supported.";
                                          } else if (error) {
                                              alertText = [NSString stringWithFormat:@"error: domain = %@, code = %d", error.domain, error.code];
                                          } else if (result == FBNativeDialogResultSucceeded) {
                                              alertText = @"Posted successfully.";
                                          }
                                          
                                          if (![alertText isEqualToString:@""]) {
                                              // Show the result in an alert
                                              [[[UIAlertView alloc] initWithTitle:@"Result"
                                                                          message:alertText
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK!"
                                                                otherButtonTitles:nil]
                                               show];
                                          }
                                      }];
        if (!displayedNativeDialog) {
            /*
             Fallback to web-based Feed dialog:
             https://developers.facebook.com/docs/howtos/feed-dialog-using-ios-sdk/
             */
        }
    }];
    [actionSheet addButtonWithTitle:@"Twitter" block:^{
        NSLog(@"Twitter");
        [self onTwitter];
    }];
    //[actionSheet cancelButtonWithTitle:@"Cancel" block:nil];
    actionSheet.cornerRadius = 5;
    
    [actionSheet showWithTouch:event];
}

-(void)onTwitter{
    NSString * strImage = objNews.image_url;
    NSString * strLink = objNews.url;
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
        [tweetSheet setInitialText:@"Share from TAC App!"];
        
        if (strImage)
        {
            //[tweetSheet addImage:<#(UIImage *)#>];
            //[tweetSheet addImage:<#(UIImage *)#>]
        }
        
        if (strLink)
        {
            [tweetSheet addURL:[NSURL URLWithString:strLink]];
        }
        
	    [self presentModalViewController:tweetSheet animated:YES];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                            message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)onFacebook{
    //[delegate fbShare:objNews];
    BOOL displayedNativeDialog = [FBNativeDialogs
                                  presentShareDialogModallyFrom:self
                                  initialText:@"sharring from TAC"
                                  image:[UIImage imageNamed:@"testimage.png"]
                                  url:[NSURL URLWithString:objNews.url]
                                  handler:^(FBNativeDialogResult result, NSError *error) {
                                      
                                      NSString *alertText = @"";
                                      if ([[error userInfo][FBErrorDialogReasonKey] isEqualToString:FBErrorDialogNotSupported]) {
                                          alertText = @"Sharing is not supported not supported.";
                                      } else if (error) {
                                          alertText = [NSString stringWithFormat:@"error: domain = %@, code = %d", error.domain, error.code];
                                      } else if (result == FBNativeDialogResultSucceeded) {
                                          alertText = @"Hooray! Successfully shared.";
                                      }
                                      
                                      if (![alertText isEqualToString:@""]) {
                                          // Show the result in an alert
                                          [[[UIAlertView alloc] initWithTitle:@"Result"
                                                                      message:alertText
                                                                     delegate:self
                                                            cancelButtonTitle:@"OK!"
                                                            otherButtonTitles:nil]
                                           show];
                                      }
                                  }];
    if (!displayedNativeDialog) {
        /*
         Fallback to web-based Feed dialog:
         https://developers.facebook.com/docs/howtos/feed-dialog-using-ios-sdk/
         */
    }
}

#pragma mark Custom Button Methods
- (void)showShareActionSheet:(id)sender //Define method to show action sheet
{
    NSString *actionSheetTitle = @"Share"; //Action Sheet Title
    //NSString *destructiveTitle = ; //Action Sheet Button Titles
    NSString *other1 = @"Twitter";
    NSString *other2 = @"Facebook";
    NSString *cancelTitle = @"Cancel";
    if (!shareActionSheet) {
        shareActionSheet = [[UIActionSheet alloc] initWithTitle:actionSheetTitle
                                                        delegate:self
                                               cancelButtonTitle:cancelTitle
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:other1, other2, nil];
    }
    
    
    [shareActionSheet showInView:self.view];
}

#pragma mark -
#pragma mark UIActionSheetDelegate Methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([buttonTitle isEqualToString:@"Destructive Button"]) {
        NSLog(@"Destructive pressed --> Delete Something");
    }
    if ([buttonTitle isEqualToString:@"Twitter"]) {
        NSLog(@"Other 1 pressed");
        [self onTwitter];
    }
    if ([buttonTitle isEqualToString:@"Facebook"]) {
        NSLog(@"Other 2 pressed");
        [self onFacebook];
    }
    if ([buttonTitle isEqualToString:@"Other Button 3"]) {
        NSLog(@"Other 3 pressed");
    }
    if ([buttonTitle isEqualToString:@"Cancel"]) {
        NSLog(@"Cancel pressed --> Cancel ActionSheet");
    }
    
}


#pragma mark - UIScrollViewDelegate Protocol Methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSLog(@"%@:%@", [self class], NSStringFromSelector(_cmd));
}

#pragma mark - Internal Methods

- (void)handleTap:(UIGestureRecognizer *)gesture {
    NSLog(@"%@:%@", [self class], NSStringFromSelector(_cmd));
}

- (void)viewWillDisappear:(BOOL)animated{
    
}

- (void)viewDidDisappear:(BOOL)animated{
    [parallaxView removeFromSuperview];
    parallaxView = nil;
    [self.navigationController popViewControllerAnimated:YES];
}
@end
