//
//  CustomScrollSliderView.h
//  IACTAC
//
//  Created by Zayar on 10/31/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfinitePagingView.h"
#import "SliderItem.h"
typedef enum {
	ScrollViewModeNotInitialized2,           // view has just been loaded
	ScrollViewModePaging2,                   // fully zoomed out, swiping enabled
	ScrollViewModeZooming2,                  // zoomed in, panning enabled
} ScrollViewMode2;
@protocol CustomScrollSliderViewDelegate;
@interface CustomScrollSliderView : UIView
{
    UIScrollView * scrollView;
    UIPageControl * pageControl;
    ScrollViewMode2 scrollViewMode2;
    BOOL pageControlUsed;
    int currentPagingIndex;
    NSMutableArray * arrContentView;
    NSTimer * myTimer;
    int slidingIndex;
    InfinitePagingView * infinitePageView;
    int totalCount;
    NSInteger _lastPageIndex;
    id<CustomScrollSliderViewDelegate> owner;
}
- (void) loadImageViewWith:(NSArray *)arr;
@property (nonatomic) id<CustomScrollSliderViewDelegate> owner;
@end


@protocol CustomScrollSliderViewDelegate
- (void) onImageClick:(CustomScrollSliderView *)sliderView andObjectImage:(SliderItem *) sItem;
@end

